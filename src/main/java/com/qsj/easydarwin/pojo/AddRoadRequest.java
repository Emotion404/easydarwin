package com.qsj.easydarwin.pojo;

import com.qsj.easydarwin.pojo.base.Road;

public class AddRoadRequest {
    private String cameraId;

    private Road road;

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public Road getRoad() {
        return road;
    }

    public void setRoad(Road road) {
        this.road = road;
    }
}
