package com.qsj.easydarwin.pojo;

import javax.persistence.*;

import lombok.Data;
import lombok.ToString;

/**
 * 点位配置
 */
@ToString
@Data
@Table(name = "camera_config")
public class CameraConfig {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 配置名称
     */
    @Column(name = "config_name")
    private String configName;

    @Column(name = "rtsp")
    private String rtsp;

    /**
     * 车流量
     */
    @Column(name = "`traffic_flow_open`")
    private Boolean trafficFlowOpen;

    /**
     * 开始时间
     */
    @Column(name = "`traffic_flow_start`")
    private String trafficFlowStart;

    /**
     * 结束时间
     */
    @Column(name = "`traffic_flow_end`")
    private String trafficFlowEnd;

    /**
     * 车流量推送间隔
     */
    @Column(name = "`traffic_flow_interval`")
    private Integer trafficFlowInterval;

    /**
     * 异常停车
     */
    @Column(name = "park_open")
    private Boolean parkOpen;

    @Column(name = "park_start")
    private String parkStart;

    @Column(name = "park_end")
    private String parkEnd;

    /**
     * 异常停车判定帧数
     */
    @Column(name = "park_frame_number")
    private Integer parkFrameNumber;

    /**
     * 违禁车辆
     */
    @Column(name = "prohibited_vehicles_open")
    private Boolean prohibitedVehiclesOpen;

    @Column(name = "prohibited_vehicles_start")
    private String prohibitedVehiclesStart;

    @Column(name = "prohibited_vehicles_end")
    private String prohibitedVehiclesEnd;

    /**
     * 违法行驶
     */
    @Column(name = "illegal_driving_open")
    private Boolean illegalDrivingOpen;

    @Column(name = "illegal_driving_start")
    private String illegalDrivingStart;

    @Column(name = "illegal_driving_end")
    private String illegalDrivingEnd;

    /**
     * 逆行距离(像素
     */
    @Column(name = "retrograde")
    private Integer retrograde;

    /**
     * 应急车道判断帧数
     */
    @Column(name = "emergency_line_frame_number")
    private Integer emergencyLineFrameNumber;

    /**
     * 行人或非机动车
     */
    @Column(name = "people_open")
    private Boolean peopleOpen;

    @Column(name = "people_start")
    private String peopleStart;

    @Column(name = "people_end")
    private String peopleEnd;

    /**
     * 行人判断帧数
     */
    @Column(name = "people_frame_number")
    private Integer peopleFrameNumber;

    /**
     * 非机动车判断帧数
     */
    @Column(name = "no_motor_vehicle_frame_number")
    private Integer noMotorVehicleFrameNumber;

    /**
     * 抛洒物
     */
    @Column(name = "throwing_open")
    private Boolean throwingOpen;

    @Column(name = "throwing_start")
    private String throwingStart;

    @Column(name = "throwing_end")
    private String throwingEnd;

    /**
     * 抛洒物判定帧数
     */
    @Column(name = "throwing_frame_number")
    private Integer throwingFrameNumber;
    /**
     * 拥堵
     */
    @Column(name = "traffic_jam_open")
    private Boolean trafficJamOpen;

    @Column(name = "traffic_jam_start")
    private String trafficJamStart;

    @Column(name = "traffic_jam_end")
    private String trafficJamEnd;

    /**
     * 施工区域
     */
    @Column(name = "construction_open")
    private Boolean constructionOpen;

    @Column(name = "construction_start")
    private String constructionStart;

    @Column(name = "construction_end")
    private String constructionEnd;

    /**
     * 三角锥个数
     */
    @Column(name = "triangular_cone_number")
    private Integer triangularConeNumber;

    /**
     * 火灾
     */
    @Column(name = "fire_open")
    private Boolean fireOpen;

    @Column(name = "fire_start")
    private String fireStart;

    @Column(name = "fire_end")
    private String fireEnd;

    /**
     * 团雾
     */
    @Column(name = "fog_open")
    private Boolean fogOpen;

    @Column(name = "fog_start")
    private String fogStart;

    @Column(name = "fog_end")
    private String fogEnd;

    /**
     * 视频质量
     */
    @Column(name = "video_quality_open")
    private Boolean videoQualityOpen;

    @Column(name = "video_quality_start")
    private String videoQualityStart;

    @Column(name = "video_quality_end")
    private String videoQualityEnd;
}