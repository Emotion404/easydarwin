package com.qsj.easydarwin.pojo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@ExcelIgnoreUnannotated
@Data
@Table(name = "t_video_analysis")
public class VideoAnalysis {
    @Id
    @Column(name = "id")
    private Long id;

    /**
     * 视频名称
     */
    @ExcelProperty(value = "视频名称",index = 0)
    @Column(name = "video_name")
    private String videoName;

    /**
     * 摄像机点位名称
     */
    @ExcelProperty(value = "摄像机点位名称",index = 1)
    @Column(name = "camera_name")
    private String cameraName;

    /**
     * 物品类型
     */
    @ColumnWidth(40)
    @ExcelProperty(value = "物品类型",index = 2)
    @Column(name = "goods")
    private String goods;

    /**
     * 事件类型
     */
    @ColumnWidth(40)
    @ExcelProperty(value = "事件类型",index = 3)
    @Column(name = "event")
    private String event;

    /**
     * 视频时间点
     */
    @ExcelProperty(value = "视频时间点",index = 4)
    @Column(name = "`time`")
    private String time;

    /**
     * 天气情况
     */
    @ExcelProperty(value = "天气情况",index = 5)
    @Column(name = "weather")
    private String weather;

    /**
     * 场景
     */
    @ExcelProperty(value = "场景",index = 6)
    @Column(name = "scene")
    private String scene;

    @ExcelProperty(value = "图片",index = 7)
    @Column(name = "img_url")
    private String imgUrl;
}