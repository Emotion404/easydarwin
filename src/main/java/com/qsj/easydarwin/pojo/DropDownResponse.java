package com.qsj.easydarwin.pojo;

public class DropDownResponse {
    //值
    private String value;
    //标签
    private String label;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}