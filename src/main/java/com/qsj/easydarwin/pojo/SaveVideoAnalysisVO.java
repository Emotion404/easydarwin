package com.qsj.easydarwin.pojo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/7 16:37
 */
@ToString
@Data
public class SaveVideoAnalysisVO {
    private String videoName;
    private String cameraName;
    private List<String> goods;
    private List<String> event;
    private String scene;
    private String time;
    private String weather;
    private String imgUrl;
}
