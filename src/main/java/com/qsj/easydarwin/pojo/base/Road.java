package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 路段
 */
@Component
public class Road {
    //道路编号
    private String id;
    //道路名称
    private String name;
    //方向
    private Direction direction;
    //交通事件配置
    private EventConfigs eventConfigs;
    //交通参数配置
    private TrafficConfig trafficConfig;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public EventConfigs getEventConfigs() {
        return eventConfigs;
    }

    public void setEventConfigs(EventConfigs eventConfigs) {
        this.eventConfigs = eventConfigs;
    }

    public TrafficConfig getTrafficConfig() {
        return trafficConfig;
    }

    public void setTrafficConfig(TrafficConfig trafficConfig) {
        this.trafficConfig = trafficConfig;
    }
}
