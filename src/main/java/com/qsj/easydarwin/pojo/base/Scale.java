package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 刻度
 */
@Component
public class Scale {
    //最小刻度 单位米
    private float mini;
    //刻度线
    private List<Line> lines;

    public float getMini() {
        return mini;
    }

    public void setMini(float mini) {
        this.mini = mini;
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    @Override
    public String toString() {
        return "Scale{" +
                "mini=" + mini +
                ", lines=" + lines +
                '}';
    }
}
