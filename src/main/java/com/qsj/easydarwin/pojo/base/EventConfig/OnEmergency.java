package com.qsj.easydarwin.pojo.base.EventConfig;


import com.qsj.easydarwin.pojo.base.Area;

import java.util.List;

public class OnEmergency {
    //区域
    private List<Area> areas;
    //停留时间 单位秒
    private int num;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
