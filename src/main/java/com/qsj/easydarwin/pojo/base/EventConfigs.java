package com.qsj.easydarwin.pojo.base;


import com.qsj.easydarwin.pojo.base.EventConfig.*;

public class EventConfigs {
    //异常停车
    private IllegalPark illegalPark;
    //违禁车辆
    private ForbidCar forbidCar;
    //违法行使
    private IllegalDriving illegalDriving;
    //行人或非机动车
    private PeopleOrNoVehicles peopleOrNoVehicles;
    //抛洒物
    private ThrowThings throwThings;
    //拥堵
    private Crowed crowed;
    //施工区域
    private WorkSpace workSpace;
    //火灾
    private Fire fire;
    //团雾
    private Fog fog;
    //视频质量
    private Video video;

    public IllegalPark getIllegalPark() {
        return illegalPark;
    }

    public void setIllegalPark(IllegalPark illegalPark) {
        this.illegalPark = illegalPark;
    }

    public ForbidCar getForbidCar() {
        return forbidCar;
    }

    public void setForbidCar(ForbidCar forbidCar) {
        this.forbidCar = forbidCar;
    }

    public IllegalDriving getIllegalDriving() {
        return illegalDriving;
    }

    public void setIllegalDriving(IllegalDriving illegalDriving) {
        this.illegalDriving = illegalDriving;
    }

    public PeopleOrNoVehicles getPeopleOrNoVehicles() {
        return peopleOrNoVehicles;
    }

    public void setPeopleOrNoVehicles(PeopleOrNoVehicles peopleOrNoVehicles) {
        this.peopleOrNoVehicles = peopleOrNoVehicles;
    }

    public ThrowThings getThrowThings() {
        return throwThings;
    }

    public void setThrowThings(ThrowThings throwThings) {
        this.throwThings = throwThings;
    }

    public Crowed getCrowed() {
        return crowed;
    }

    public void setCrowed(Crowed crowed) {
        this.crowed = crowed;
    }

    public WorkSpace getWorkSpace() {
        return workSpace;
    }

    public void setWorkSpace(WorkSpace workSpace) {
        this.workSpace = workSpace;
    }

    public Fire getFire() {
        return fire;
    }

    public void setFire(Fire fire) {
        this.fire = fire;
    }

    public Fog getFog() {
        return fog;
    }

    public void setFog(Fog fog) {
        this.fog = fog;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
