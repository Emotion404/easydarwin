package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 服务时段 24小时制
 */
@Component
public class ServiceTime {
    //开始时间 例如7:00
    private String startTime;
    //结束时间 例如19:00
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ServiceTime{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
