package com.qsj.easydarwin.pojo.base.EventConfig;


import com.qsj.easydarwin.pojo.base.Area;
import com.qsj.easydarwin.pojo.base.ServiceTime;

import java.util.List;

public class ThrowThings {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;
    //区域
    private List<Area> areas;
    //静止时间 单位秒
    private int num;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
