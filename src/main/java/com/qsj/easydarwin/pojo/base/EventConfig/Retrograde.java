package com.qsj.easydarwin.pojo.base.EventConfig;

import com.qsj.easydarwin.pojo.base.Area;

import java.util.List;

public class Retrograde {
    //区域
    private List<Area> areas;
    //图片中反向像素距离
    private int pixel;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getPixel() {
        return pixel;
    }

    public void setPixel(int pixel) {
        this.pixel = pixel;
    }
}
