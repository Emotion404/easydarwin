package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 线段
 */
@Component
public class Line {
    //A端点
    private Point pointA;
    //B端点
    private Point pointB;

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    @Override
    public String toString() {
        return "Line{" +
                "pointA=" + pointA +
                ", pointB=" + pointB +
                '}';
    }
}
