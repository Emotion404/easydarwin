package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 区域
 */
@Component
public class Area {
    //A端点
    private Point pointA;
    //B端点
    private Point pointB;
    //C端点
    private Point pointC;
    //D端点
    private Point pointD;

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public Point getPointD() {
        return pointD;
    }

    public void setPointD(Point pointD) {
        this.pointD = pointD;
    }
}
