package com.qsj.easydarwin.pojo.base.EventConfig;


import com.qsj.easydarwin.pojo.base.ServiceTime;

public class Video {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }
}
