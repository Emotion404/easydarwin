package com.qsj.easydarwin.pojo.base;

/**
 * @author wuzhoujian
 * @date 2020/6/18 21:01
 */
public class Coordinate {
    private int obj_ID;
    private double prob;
    private int xAxis;
    private int yAxis;
    private int height;
    private int width;

    public int getObj_ID() {
        return obj_ID;
    }

    public void setObj_ID(int obj_ID) {
        this.obj_ID = obj_ID;
    }

    public double getProb() {
        return prob;
    }

    public void setProb(double prob) {
        this.prob = prob;
    }

    public int getxAxis() {
        return xAxis;
    }

    public void setxAxis(int xAxis) {
        this.xAxis = xAxis;
    }

    public int getyAxis() {
        return yAxis;
    }

    public void setyAxis(int yAxis) {
        this.yAxis = yAxis;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
