package com.qsj.easydarwin.pojo.base.EventConfig;



import com.qsj.easydarwin.pojo.base.AreaForCrowed;
import com.qsj.easydarwin.pojo.base.ServiceTime;

import java.util.List;

public class Crowed {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;
    //区域
    private List<AreaForCrowed> areas;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public List<AreaForCrowed> getAreas() {
        return areas;
    }

    public void setAreas(List<AreaForCrowed> areas) {
        this.areas = areas;
    }
}
