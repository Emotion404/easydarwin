package com.qsj.easydarwin.pojo.base;

public class CameraDetails {
    //名称
    private String name;
    //厂家
    private String manufacturer;
    //型号
    private String model;
    //样式
    private String style;
    //状态 0停用 1启用
    private int status;
    //ip
    private String ip;
    //端口
    private String port;
    //场景照
    private String sceneImg;
    //场景照地址
    private String sceneImgUrl;
    //rtsp视频链接
    private String rtsp;
    //事件保存路径
    private String savePath;
    //事件保留天数
    private int saveDays;
    //经度
    private double longitude;
    //纬度
    private double latitude;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSceneImg() {
        return sceneImg;
    }

    public void setSceneImg(String sceneImg) {
        this.sceneImg = sceneImg;
    }

    public String getSceneImgUrl() {
        return sceneImgUrl;
    }

    public void setSceneImgUrl(String sceneImgUrl) {
        this.sceneImgUrl = sceneImgUrl;
    }

    public String getRtsp() {
        return rtsp;
    }

    public void setRtsp(String rtsp) {
        this.rtsp = rtsp;
    }

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public int getSaveDays() {
        return saveDays;
    }

    public void setSaveDays(int saveDays) {
        this.saveDays = saveDays;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
