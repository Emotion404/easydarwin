package com.qsj.easydarwin.pojo.base.EventConfig;


import com.qsj.easydarwin.pojo.base.ServiceTime;

public class IllegalDriving {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;
    //逆行
    private Retrograde retrograde;
    //行驶应急车道
    private OnEmergency onEmergency;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public Retrograde getRetrograde() {
        return retrograde;
    }

    public void setRetrograde(Retrograde retrograde) {
        this.retrograde = retrograde;
    }

    public OnEmergency getOnEmergency() {
        return onEmergency;
    }

    public void setOnEmergency(OnEmergency onEmergency) {
        this.onEmergency = onEmergency;
    }
}
