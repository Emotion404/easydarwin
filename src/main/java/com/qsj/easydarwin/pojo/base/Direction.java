package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 方向
 */
@Component
public class Direction {
    //起始点
    private Point startPoint;
    //结束点
    private Point endPoint;

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "startPoint=" + startPoint +
                ", endPoint=" + endPoint +
                '}';
    }
}
