package com.qsj.easydarwin.pojo.base;

import java.util.List;

public class TrafficConfig {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;
    //区域
    private List<Area> areas;
    //记录时间间隔 单位秒
    private int interval;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
