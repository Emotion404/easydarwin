package com.qsj.easydarwin.pojo.base.EventConfig;


import com.qsj.easydarwin.pojo.base.ServiceTime;

public class PeopleOrNoVehicles {
    //是否开启
    private boolean isOpen;
    //服务时段
    private ServiceTime serviceTime;
    //行人
    private People people;
    //非机动车
    private NoVehicles noVehicles;

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ServiceTime getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(ServiceTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public NoVehicles getNoVehicles() {
        return noVehicles;
    }

    public void setNoVehicles(NoVehicles noVehicles) {
        this.noVehicles = noVehicles;
    }
}
