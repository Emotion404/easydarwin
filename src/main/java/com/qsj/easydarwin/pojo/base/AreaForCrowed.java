package com.qsj.easydarwin.pojo.base;

/**
 * @author wuzhoujian
 * @date 2020/5/14 10:07
 */
public class AreaForCrowed {
    //拥堵车道名称
    private String crowedName;
    //A端点
    private Point pointA;
    //B端点
    private Point pointB;
    //C端点
    private Point pointC;
    //D端点
    private Point pointD;
    //车辆数阈值
    private int count;
    //检测区域车道长度
    private int distance;

    public String getCrowedName() {
        return crowedName;
    }

    public void setCrowedName(String crowedName) {
        this.crowedName = crowedName;
    }

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public Point getPointD() {
        return pointD;
    }

    public void setPointD(Point pointD) {
        this.pointD = pointD;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
