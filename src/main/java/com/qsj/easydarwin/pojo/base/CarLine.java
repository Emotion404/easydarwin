package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 车道线
 */
@Component
public class CarLine {
    //线段
    private Line line;

    //类型 edge边缘线 boundary分界线
    private String type;

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CarLine{" +
                "line=" + line +
                ", type='" + type + '\'' +
                '}';
    }
}
