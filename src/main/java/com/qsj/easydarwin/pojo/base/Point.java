package com.qsj.easydarwin.pojo.base;

import org.springframework.stereotype.Component;

/**
 * 点
 */
@Component
public class Point {
    //x坐标
    private int xAxis;
    //y坐标
    private int yAxis;

    public int getxAxis() {
        return xAxis;
    }

    public void setxAxis(int xAxis) {
        this.xAxis = xAxis;
    }

    public int getyAxis() {
        return yAxis;
    }

    public void setyAxis(int yAxis) {
        this.yAxis = yAxis;
    }

    @Override
    public String toString() {
        return "Point{" +
                "xAxis=" + xAxis +
                ", yAxis=" + yAxis +
                '}';
    }
}
