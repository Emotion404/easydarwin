package com.qsj.easydarwin.pojo.base;

import java.util.List;

public class BasePageResponse<T> {
    //总条数
    private int count;
    //列表数据
    private List<T> list;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
