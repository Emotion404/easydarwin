package com.qsj.easydarwin.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/16 15:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileListResponse {
    private String name;
    private String path;
    private String url;
    private String rtsp;
    private boolean visited;
}
