package com.qsj.easydarwin.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/22 15:00
 */
@ToString
@Data
public class CameraConfigVO {
    private Integer id;

    /**
     * 配置名称
     */
    private String configName;

    private String rtsp;

    /**
     * 车流量
     */
    private Boolean trafficFlowOpen = false;

    /**
     * 开始时间
     */
    private String trafficFlowTime;
    /**
     * 车流量推送间隔
     */
    private Integer trafficFlowInterval;

    /**
     * 异常停车
     */
    private Boolean parkOpen = false;

    private String parkTime;

    /**
     * 异常停车判定帧数
     */
    private Integer parkFrameNumber;

    /**
     * 违禁车辆
     */
    private Boolean prohibitedVehiclesOpen = false;

    private String prohibitedVehiclesTime;

    /**
     * 违法行驶
     */
    private Boolean illegalDrivingOpen = false;

    private String illegalDrivingTime;

    /**
     * 逆行距离(像素
     */
    private Integer retrograde;

    /**
     * 应急车道判断帧数
     */
    private Integer emergencyLineFrameNumber;

    /**
     * 行人或非机动车
     */
    private Boolean peopleOpen = false;

    private String peopleTime;

    /**
     * 行人判断帧数
     */
    private Integer peopleFrameNumber;

    /**
     * 非机动车判断帧数
     */
    private Integer noMotorVehicleFrameNumber;

    /**
     * 抛洒物
     */
    private Boolean throwingOpen = false;

    private String throwingTime;
    /**
     * 抛洒物判定帧数
     */
    private Integer throwingFrameNumber;
    /**
     * 拥堵
     */
    private Boolean trafficJamOpen = false;

    private String trafficJamTime;
    /**
     * 施工区域
     */
    private Boolean constructionOpen = false;

    private String constructionTime;

    /**
     * 三角锥个数
     */
    private Integer triangularConeNumber;

    /**
     * 火灾
     */
    private Boolean fireOpen = false;

    private String  fireTime;

    /**
     * 团雾
     */
    private Boolean fogOpen = false;

    private String fogTime;
    /**
     * 视频质量
     */
    private Boolean videoQualityOpen = false;

    private String videoQualityTime;
}
