package com.qsj.easydarwin.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/22 14:15
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class CameraConfigListResponse {
    private int id;
    private String configName;
}
