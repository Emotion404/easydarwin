package com.qsj.easydarwin.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/21 17:01
 */
@ToString
@Data
public class Point {
    int x;
    int y;
}
