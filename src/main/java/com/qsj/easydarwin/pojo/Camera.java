package com.qsj.easydarwin.pojo;

import com.qsj.easydarwin.pojo.base.CameraDetails;
import com.qsj.easydarwin.pojo.base.Road;
import com.qsj.easydarwin.pojo.base.TargetThreshold;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class Camera {
    private String id;
    //摄像头信息
    private CameraDetails cameraDetails;
    //目标阈值
    private List<TargetThreshold> targetThreshold;
    //路段信息
    private List<Road> roads;
}
