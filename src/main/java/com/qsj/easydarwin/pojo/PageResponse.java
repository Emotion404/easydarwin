package com.qsj.easydarwin.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/23 13:34
 */
@Data
public class PageResponse<T> {
    private int totalCount;
    private List<T> data;
}
