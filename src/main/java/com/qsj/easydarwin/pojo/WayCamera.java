package com.qsj.easydarwin.pojo;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

/**
    * 点位
    */
@Data
@Table(name = "way_camera")
public class WayCamera {
    /**
     * id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 设备id
     */
    @Column(name = "camera_server_id")
    private Integer cameraServerId=1;

    /**
     * 桩号
     */
    @Column(name = "`name`")
    private String name;

    /**
     * 用户名
     */
    @Column(name = "username")
    private String username="admin";

    /**
     * 密码
     */
    @Column(name = "`password`")
    private String password="admin";

    /**
     * 点位ip
     */
    @Column(name = "ip")
    private String ip="127.0.0.1";

    /**
     * 端口
     */
    @Column(name = "port")
    private String port="777";

    /**
     * 方向
     */
    @Column(name = "direction")
    private String direction="in";

    /**
     * 是否启用
     */
    @Column(name = "is_enabled")
    private Boolean isEnabled=true;

    /**
     * 厂家
     */
    @Column(name = "manufacturer")
    private String manufacturer;

    /**
     * 型号
     */
    @Column(name = "model")
    private String model;

    /**
     * 样式
     */
    @Column(name = "`style`")
    private String style = "定点定焦摄像机";

    /**
     * 图片存储路径
     */
    @Column(name = "img_path")
    private String imgPath;

    /**
     * 图片地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 视频流
     */
    @Column(name = "rtsp")
    private String rtsp;

    /**
     * 创建者
     */
    @Column(name = "create_id")
    private Integer createId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @Column(name = "update_id")
    private Integer updateId;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @Column(name = "remarks")
    private String remarks;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Boolean isDelete;
}