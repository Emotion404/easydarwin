package com.qsj.easydarwin.pojo;

import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/22 17:29
 */
public class ConfigResponse {

    /**
     * code : 200
     * success : true
     * timeStamp : 1595409635845
     * msg : 操作成功
     * data : [{"id":3,"cameraDetails":{"name":"test03","manufacturer":null,"model":null,"style":"定点定焦摄像机","ip":"192.168.1.20","port":null,"rtsp":"rtsp://192.168.1.20/test1239-202004211910.mp4","saveDays":"7","savePath":"/home/xsy/projects/webconfigs/tomcat/file/test03","sceneImg":"http://192.168.1.27/api/resource/getcameraimage/d3aed20cfcd64f89b401d9bab55e0c12","status":1,"kafkaIp":"192.168.1.100","kafkaPort":"9092","kafkaTopic":"test_topic"},"targetThreshold":[{"code":"Person","description":"人","threshold":0.6},{"code":"Bus","description":"客车","threshold":0.6},{"code":"Car","description":"小轿车","threshold":0.6},{"code":"Barricade","description":"三角锥","threshold":0.6},{"code":"Cup","description":"杯子","threshold":0.6},{"code":"Motorbike","description":"电动车","threshold":0.6},{"code":"Truck","description":"卡车","threshold":0.6},{"code":"Bicycle","description":"自行车","threshold":0.6},{"code":"Cans","description":"易拉罐","threshold":0.6},{"code":"Bag","description":"塑料袋","threshold":0.6},{"code":"Box","description":"纸盒子","threshold":0.6},{"code":"Bottle","description":"矿泉水瓶","threshold":0.6},{"code":"Threebicycle","description":"三轮车","threshold":0.6},{"code":"TZC","description":"特种车","threshold":0.6},{"code":"Tanker","description":"油罐车","threshold":0.6},{"code":"MealBox","description":"塑料快餐盒","threshold":0.6},{"code":"BullBarrels","description":"防撞桶","threshold":0.6}],"roads":[{"eventConfigs":{"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":251,"xAxis":1096},"pointA":{"yAxis":752,"xAxis":128},"pointB":{"yAxis":911,"xAxis":1487},"pointC":{"yAxis":292,"xAxis":1552}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":5,"areas":[{"pointD":{"yAxis":975,"xAxis":1345},"pointA":{"yAxis":825,"xAxis":88},"pointB":{"yAxis":339,"xAxis":999},"pointC":{"yAxis":368,"xAxis":1493}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":312,"xAxis":1012},"pointA":{"yAxis":771,"xAxis":185},"pointB":{"yAxis":948,"xAxis":537},"pointC":{"yAxis":352,"xAxis":1153}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":280,"xAxis":1293},"pointA":{"yAxis":965,"xAxis":599},"pointB":{"yAxis":991,"xAxis":1351},"pointC":{"yAxis":315,"xAxis":1509}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":197,"xAxis":1232},"pointA":{"yAxis":617,"xAxis":451},"pointB":{"yAxis":747,"xAxis":1421},"pointC":{"yAxis":225,"xAxis":1537}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":219,"xAxis":1161},"pointA":{"yAxis":805,"xAxis":72},"pointB":{"yAxis":1044,"xAxis":1439},"pointC":{"yAxis":272,"xAxis":1553}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}},"name":"布控1","id":4,"trafficConfig":{"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"direction":{"endPoint":{"yAxis":482,"xAxis":1068},"startPoint":{"yAxis":611,"xAxis":937}}}]},{"id":2,"cameraDetails":{"name":"test02","manufacturer":null,"model":null,"style":"定点定焦摄像机","ip":"192.168.1.2","port":"80","rtsp":"rtsp://192.168.1.20/test1251-202004211400.mp4","saveDays":"7","savePath":"/home/xsy/projects/webconfigs/tomcat/file/test02","sceneImg":"http://192.168.1.27/api/resource/getcameraimage/0b2693d1f5fa4d78a876b4b2199a6ea8","status":1,"kafkaIp":"192.168.1.100","kafkaPort":"9092","kafkaTopic":"test_topic"},"targetThreshold":[{"code":"Person","description":"人","threshold":0.6},{"code":"Bus","description":"客车","threshold":0.6},{"code":"Car","description":"小轿车","threshold":0.6},{"code":"Barricade","description":"三角锥","threshold":0.6},{"code":"Cup","description":"杯子","threshold":0.6},{"code":"Motorbike","description":"电动车","threshold":0.6},{"code":"Truck","description":"卡车","threshold":0.6},{"code":"Bicycle","description":"自行车","threshold":0.6},{"code":"Cans","description":"易拉罐","threshold":0.6},{"code":"Bag","description":"塑料袋","threshold":0.6},{"code":"Box","description":"纸盒子","threshold":0.6},{"code":"Bottle","description":"矿泉水瓶","threshold":0.6},{"code":"Threebicycle","description":"三轮车","threshold":0.6},{"code":"TZC","description":"特种车","threshold":0.6},{"code":"Tanker","description":"油罐车","threshold":0.6},{"code":"MealBox","description":"塑料快餐盒","threshold":0.6},{"code":"BullBarrels","description":"防撞桶","threshold":0.6}],"roads":[{"eventConfigs":{"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":1026,"xAxis":860},"pointA":{"yAxis":735,"xAxis":61},"pointB":{"yAxis":564,"xAxis":382},"pointC":{"yAxis":564,"xAxis":933}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":3,"areas":[{"pointD":{"yAxis":904,"xAxis":175},"pointA":{"yAxis":847,"xAxis":63},"pointB":{"yAxis":634,"xAxis":417},"pointC":{"yAxis":647,"xAxis":534}},{"pointD":{"yAxis":977,"xAxis":740},"pointA":{"yAxis":892,"xAxis":270},"pointB":{"yAxis":512,"xAxis":746},"pointC":{"yAxis":509,"xAxis":877}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[{"pointD":{"yAxis":781,"xAxis":20},"pointA":{"yAxis":747,"xAxis":6},"pointB":{"yAxis":474,"xAxis":535},"pointC":{"yAxis":491,"xAxis":549}}]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":947,"xAxis":836},"pointA":{"yAxis":770,"xAxis":1},"pointB":{"yAxis":510,"xAxis":495},"pointC":{"yAxis":489,"xAxis":931}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":804,"xAxis":239},"pointA":{"yAxis":768,"xAxis":87},"pointB":{"yAxis":534,"xAxis":504},"pointC":{"yAxis":534,"xAxis":598}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":824,"xAxis":432},"pointA":{"yAxis":806,"xAxis":236},"pointB":{"yAxis":526,"xAxis":668},"pointC":{"yAxis":535,"xAxis":743}},{"distance":40,"crowedName":"车道3","count":3,"pointD":{"yAxis":837,"xAxis":415},"pointA":{"yAxis":860,"xAxis":613},"pointB":{"yAxis":543,"xAxis":832},"pointC":{"yAxis":546,"xAxis":743}},{"distance":40,"crowedName":"车道4","count":3,"pointD":{"yAxis":869,"xAxis":854},"pointA":{"yAxis":861,"xAxis":616},"pointB":{"yAxis":543,"xAxis":834},"pointC":{"yAxis":536,"xAxis":926}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":1042,"xAxis":825},"pointA":{"yAxis":760,"xAxis":7},"pointB":{"yAxis":493,"xAxis":522},"pointC":{"yAxis":485,"xAxis":941}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":1044,"xAxis":816},"pointA":{"yAxis":761,"xAxis":3},"pointB":{"yAxis":491,"xAxis":518},"pointC":{"yAxis":487,"xAxis":935}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":931,"xAxis":855},"pointA":{"yAxis":735,"xAxis":154},"pointB":{"yAxis":544,"xAxis":494},"pointC":{"yAxis":530,"xAxis":935}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":1033,"xAxis":870},"pointA":{"yAxis":763,"xAxis":9},"pointB":{"yAxis":485,"xAxis":534},"pointC":{"yAxis":540,"xAxis":927}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}},"name":"布控1","id":2,"trafficConfig":{"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"direction":{"endPoint":{"yAxis":603,"xAxis":789},"startPoint":{"yAxis":513,"xAxis":850}}},{"eventConfigs":{"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":729,"xAxis":1917},"pointA":{"yAxis":908,"xAxis":988},"pointB":{"yAxis":539,"xAxis":977},"pointC":{"yAxis":542,"xAxis":1548}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":3,"areas":[{"pointD":{"yAxis":973,"xAxis":1663},"pointA":{"yAxis":963,"xAxis":1140},"pointB":{"yAxis":533,"xAxis":1048},"pointC":{"yAxis":524,"xAxis":1196}},{"pointD":{"yAxis":763,"xAxis":1906},"pointA":{"yAxis":857,"xAxis":1863},"pointB":{"yAxis":585,"xAxis":1499},"pointC":{"yAxis":553,"xAxis":1524}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[{"pointD":{"yAxis":559,"xAxis":1285},"pointA":{"yAxis":999,"xAxis":1886},"pointB":{"yAxis":945,"xAxis":1911},"pointC":{"yAxis":547,"xAxis":1380}}]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":726,"xAxis":1918},"pointA":{"yAxis":978,"xAxis":985},"pointB":{"yAxis":538,"xAxis":980},"pointC":{"yAxis":534,"xAxis":1519}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":907,"xAxis":1769},"pointA":{"yAxis":920,"xAxis":1512},"pointB":{"yAxis":553,"xAxis":1178},"pointC":{"yAxis":543,"xAxis":1271}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":906,"xAxis":1496},"pointA":{"yAxis":923,"xAxis":1253},"pointB":{"yAxis":554,"xAxis":1077},"pointC":{"yAxis":546,"xAxis":1165}},{"distance":40,"crowedName":"车道3","count":3,"pointD":{"yAxis":935,"xAxis":980},"pointA":{"yAxis":928,"xAxis":1258},"pointB":{"yAxis":554,"xAxis":1075},"pointC":{"yAxis":553,"xAxis":977}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[]},"people":{"num":10,"areas":[{"pointD":{"yAxis":723,"xAxis":1908},"pointA":{"yAxis":937,"xAxis":984},"pointB":{"yAxis":535,"xAxis":973},"pointC":{"yAxis":545,"xAxis":1563}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":741,"xAxis":1526},"pointA":{"yAxis":769,"xAxis":980},"pointB":{"yAxis":531,"xAxis":979},"pointC":{"yAxis":528,"xAxis":1245}},{"pointD":{"yAxis":686,"xAxis":1853},"pointA":{"yAxis":755,"xAxis":1652},"pointB":{"yAxis":543,"xAxis":1383},"pointC":{"yAxis":528,"xAxis":1537}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":720,"xAxis":1916},"pointA":{"yAxis":896,"xAxis":978},"pointB":{"yAxis":530,"xAxis":976},"pointC":{"yAxis":531,"xAxis":1538}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}},"name":"布控2","id":3,"trafficConfig":{"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"direction":{"endPoint":{"yAxis":620,"xAxis":1110},"startPoint":{"yAxis":759,"xAxis":1171}}}]},{"id":1,"cameraDetails":{"name":"test01","manufacturer":null,"model":null,"style":"定点定焦摄像机","ip":"192.168.1.20","port":"80","rtsp":"rtsp://192.168.1.20/33.65.157.32-20200520-211801.mp4","saveDays":"7","savePath":"/home/xsy/projects/webconfigs/tomcat/file/test01","sceneImg":"http://192.168.1.27/api/resource/getcameraimage/7a64ceaeb55348dd99273f0d92b624b8","status":1,"kafkaIp":"192.168.1.100","kafkaPort":"9092","kafkaTopic":"test_topic"},"targetThreshold":[{"code":"Person","description":"人","threshold":0.6},{"code":"Bus","description":"客车","threshold":0.6},{"code":"Car","description":"小轿车","threshold":0.6},{"code":"Barricade","description":"三角锥","threshold":0.6},{"code":"Cup","description":"杯子","threshold":0.6},{"code":"Motorbike","description":"电动车","threshold":0.6},{"code":"Truck","description":"卡车","threshold":0.6},{"code":"Bicycle","description":"自行车","threshold":0.6},{"code":"Cans","description":"易拉罐","threshold":0.6},{"code":"Bag","description":"塑料袋","threshold":0.6},{"code":"Box","description":"纸盒子","threshold":0.6},{"code":"Bottle","description":"矿泉水瓶","threshold":0.6},{"code":"Threebicycle","description":"三轮车","threshold":0.6},{"code":"TZC","description":"特种车","threshold":0.6},{"code":"Tanker","description":"油罐车","threshold":0.6},{"code":"MealBox","description":"塑料快餐盒","threshold":0.6},{"code":"BullBarrels","description":"防撞桶","threshold":0.6}],"roads":[{"eventConfigs":{"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":913,"xAxis":42},"pointA":{"yAxis":708,"xAxis":5},"pointB":{"yAxis":340,"xAxis":628},"pointC":{"yAxis":354,"xAxis":698}},{"pointD":{"yAxis":938,"xAxis":497},"pointA":{"yAxis":917,"xAxis":105},"pointB":{"yAxis":374,"xAxis":704},"pointC":{"yAxis":380,"xAxis":812}},{"pointD":{"yAxis":962,"xAxis":1020},"pointA":{"yAxis":973,"xAxis":538},"pointB":{"yAxis":378,"xAxis":839},"pointC":{"yAxis":376,"xAxis":974}},{"pointD":{"yAxis":977,"xAxis":1590},"pointA":{"yAxis":1023,"xAxis":1100},"pointB":{"yAxis":383,"xAxis":1000},"pointC":{"yAxis":386,"xAxis":1140}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":3,"areas":[{"pointD":{"yAxis":989,"xAxis":1149},"pointA":{"yAxis":879,"xAxis":42},"pointB":{"yAxis":327,"xAxis":701},"pointC":{"yAxis":341,"xAxis":1018}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[]},"isOpen":true,"retrograde":{"areas":[],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":552,"xAxis":485},"pointA":{"yAxis":532,"xAxis":278},"pointB":{"yAxis":299,"xAxis":686},"pointC":{"yAxis":300,"xAxis":780}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":561,"xAxis":719},"pointA":{"yAxis":548,"xAxis":492},"pointB":{"yAxis":298,"xAxis":776},"pointC":{"yAxis":295,"xAxis":877}},{"distance":40,"crowedName":"车道3","count":3,"pointD":{"yAxis":542,"xAxis":1006},"pointA":{"yAxis":558,"xAxis":729},"pointB":{"yAxis":287,"xAxis":879},"pointC":{"yAxis":286,"xAxis":989}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":1008,"xAxis":1071},"pointA":{"yAxis":701,"xAxis":1},"pointB":{"yAxis":312,"xAxis":666},"pointC":{"yAxis":304,"xAxis":977}},{"pointD":{"yAxis":1010,"xAxis":1072},"pointA":{"yAxis":335,"xAxis":980},"pointB":{"yAxis":375,"xAxis":1133},"pointC":{"yAxis":952,"xAxis":1567}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":962,"xAxis":1027},"pointA":{"yAxis":603,"xAxis":1},"pointB":{"yAxis":285,"xAxis":645},"pointC":{"yAxis":296,"xAxis":980}},{"pointD":{"yAxis":1003,"xAxis":1031},"pointA":{"yAxis":338,"xAxis":982},"pointB":{"yAxis":361,"xAxis":1217},"pointC":{"yAxis":954,"xAxis":1896}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":544,"xAxis":1014},"pointA":{"yAxis":471,"xAxis":397},"pointB":{"yAxis":296,"xAxis":702},"pointC":{"yAxis":297,"xAxis":982}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":721,"xAxis":3},"pointA":{"yAxis":323,"xAxis":660},"pointB":{"yAxis":329,"xAxis":975},"pointC":{"yAxis":942,"xAxis":1058}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}},"name":"布控1","id":1,"trafficConfig":{"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"direction":{"endPoint":{"yAxis":346,"xAxis":846},"startPoint":{"yAxis":526,"xAxis":746}}}]}]
     */

    private int code;
    private boolean success;
    private long timeStamp;
    private String msg;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * cameraDetails : {"name":"test03","manufacturer":null,"model":null,"style":"定点定焦摄像机","ip":"192.168.1.20","port":null,"rtsp":"rtsp://192.168.1.20/test1239-202004211910.mp4","saveDays":"7","savePath":"/home/xsy/projects/webconfigs/tomcat/file/test03","sceneImg":"http://192.168.1.27/api/resource/getcameraimage/d3aed20cfcd64f89b401d9bab55e0c12","status":1,"kafkaIp":"192.168.1.100","kafkaPort":"9092","kafkaTopic":"test_topic"}
         * targetThreshold : [{"code":"Person","description":"人","threshold":0.6},{"code":"Bus","description":"客车","threshold":0.6},{"code":"Car","description":"小轿车","threshold":0.6},{"code":"Barricade","description":"三角锥","threshold":0.6},{"code":"Cup","description":"杯子","threshold":0.6},{"code":"Motorbike","description":"电动车","threshold":0.6},{"code":"Truck","description":"卡车","threshold":0.6},{"code":"Bicycle","description":"自行车","threshold":0.6},{"code":"Cans","description":"易拉罐","threshold":0.6},{"code":"Bag","description":"塑料袋","threshold":0.6},{"code":"Box","description":"纸盒子","threshold":0.6},{"code":"Bottle","description":"矿泉水瓶","threshold":0.6},{"code":"Threebicycle","description":"三轮车","threshold":0.6},{"code":"TZC","description":"特种车","threshold":0.6},{"code":"Tanker","description":"油罐车","threshold":0.6},{"code":"MealBox","description":"塑料快餐盒","threshold":0.6},{"code":"BullBarrels","description":"防撞桶","threshold":0.6}]
         * roads : [{"eventConfigs":{"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":251,"xAxis":1096},"pointA":{"yAxis":752,"xAxis":128},"pointB":{"yAxis":911,"xAxis":1487},"pointC":{"yAxis":292,"xAxis":1552}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":5,"areas":[{"pointD":{"yAxis":975,"xAxis":1345},"pointA":{"yAxis":825,"xAxis":88},"pointB":{"yAxis":339,"xAxis":999},"pointC":{"yAxis":368,"xAxis":1493}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":312,"xAxis":1012},"pointA":{"yAxis":771,"xAxis":185},"pointB":{"yAxis":948,"xAxis":537},"pointC":{"yAxis":352,"xAxis":1153}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":280,"xAxis":1293},"pointA":{"yAxis":965,"xAxis":599},"pointB":{"yAxis":991,"xAxis":1351},"pointC":{"yAxis":315,"xAxis":1509}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":197,"xAxis":1232},"pointA":{"yAxis":617,"xAxis":451},"pointB":{"yAxis":747,"xAxis":1421},"pointC":{"yAxis":225,"xAxis":1537}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":219,"xAxis":1161},"pointA":{"yAxis":805,"xAxis":72},"pointB":{"yAxis":1044,"xAxis":1439},"pointC":{"yAxis":272,"xAxis":1553}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}},"name":"布控1","id":4,"trafficConfig":{"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"direction":{"endPoint":{"yAxis":482,"xAxis":1068},"startPoint":{"yAxis":611,"xAxis":937}}}]
         */

        private int id;
        private CameraDetailsBean cameraDetails;
        private List<TargetThresholdBean> targetThreshold;
        private List<RoadsBean> roads;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public CameraDetailsBean getCameraDetails() {
            return cameraDetails;
        }

        public void setCameraDetails(CameraDetailsBean cameraDetails) {
            this.cameraDetails = cameraDetails;
        }

        public List<TargetThresholdBean> getTargetThreshold() {
            return targetThreshold;
        }

        public void setTargetThreshold(List<TargetThresholdBean> targetThreshold) {
            this.targetThreshold = targetThreshold;
        }

        public List<RoadsBean> getRoads() {
            return roads;
        }

        public void setRoads(List<RoadsBean> roads) {
            this.roads = roads;
        }

        public static class CameraDetailsBean {
            /**
             * name : test03
             * manufacturer : null
             * model : null
             * style : 定点定焦摄像机
             * ip : 192.168.1.20
             * port : null
             * rtsp : rtsp://192.168.1.20/test1239-202004211910.mp4
             * saveDays : 7
             * savePath : /home/xsy/projects/webconfigs/tomcat/file/test03
             * sceneImg : http://192.168.1.27/api/resource/getcameraimage/d3aed20cfcd64f89b401d9bab55e0c12
             * status : 1
             * kafkaIp : 192.168.1.100
             * kafkaPort : 9092
             * kafkaTopic : test_topic
             */

            private String name;
            private Object manufacturer;
            private Object model;
            private String style;
            private String ip;
            private Object port;
            private String rtsp;
            private String saveDays;
            private String savePath;
            private String sceneImg;
            private int status;
            private String kafkaIp;
            private String kafkaPort;
            private String kafkaTopic;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getManufacturer() {
                return manufacturer;
            }

            public void setManufacturer(Object manufacturer) {
                this.manufacturer = manufacturer;
            }

            public Object getModel() {
                return model;
            }

            public void setModel(Object model) {
                this.model = model;
            }

            public String getStyle() {
                return style;
            }

            public void setStyle(String style) {
                this.style = style;
            }

            public String getIp() {
                return ip;
            }

            public void setIp(String ip) {
                this.ip = ip;
            }

            public Object getPort() {
                return port;
            }

            public void setPort(Object port) {
                this.port = port;
            }

            public String getRtsp() {
                return rtsp;
            }

            public void setRtsp(String rtsp) {
                this.rtsp = rtsp;
            }

            public String getSaveDays() {
                return saveDays;
            }

            public void setSaveDays(String saveDays) {
                this.saveDays = saveDays;
            }

            public String getSavePath() {
                return savePath;
            }

            public void setSavePath(String savePath) {
                this.savePath = savePath;
            }

            public String getSceneImg() {
                return sceneImg;
            }

            public void setSceneImg(String sceneImg) {
                this.sceneImg = sceneImg;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getKafkaIp() {
                return kafkaIp;
            }

            public void setKafkaIp(String kafkaIp) {
                this.kafkaIp = kafkaIp;
            }

            public String getKafkaPort() {
                return kafkaPort;
            }

            public void setKafkaPort(String kafkaPort) {
                this.kafkaPort = kafkaPort;
            }

            public String getKafkaTopic() {
                return kafkaTopic;
            }

            public void setKafkaTopic(String kafkaTopic) {
                this.kafkaTopic = kafkaTopic;
            }
        }

        public static class TargetThresholdBean {
            /**
             * code : Person
             * description : 人
             * threshold : 0.6
             */

            private String code;
            private String description;
            private double threshold;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public double getThreshold() {
                return threshold;
            }

            public void setThreshold(double threshold) {
                this.threshold = threshold;
            }
        }

        public static class RoadsBean {
            /**
             * eventConfigs : {"throwThings":{"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":251,"xAxis":1096},"pointA":{"yAxis":752,"xAxis":128},"pointB":{"yAxis":911,"xAxis":1487},"pointC":{"yAxis":292,"xAxis":1552}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"workSpace":{"isOpen":true,"count":5,"areas":[{"pointD":{"yAxis":975,"xAxis":1345},"pointA":{"yAxis":825,"xAxis":88},"pointB":{"yAxis":339,"xAxis":999},"pointC":{"yAxis":368,"xAxis":1493}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalDriving":{"onEmergency":{"num":5,"areas":[]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fire":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"video":{"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"crowed":{"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":312,"xAxis":1012},"pointA":{"yAxis":771,"xAxis":185},"pointB":{"yAxis":948,"xAxis":537},"pointC":{"yAxis":352,"xAxis":1153}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":280,"xAxis":1293},"pointA":{"yAxis":965,"xAxis":599},"pointB":{"yAxis":991,"xAxis":1351},"pointC":{"yAxis":315,"xAxis":1509}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"peopleOrNoVehicles":{"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"forbidCar":{"isOpen":true,"areas":[{"pointD":{"yAxis":197,"xAxis":1232},"pointA":{"yAxis":617,"xAxis":451},"pointB":{"yAxis":747,"xAxis":1421},"pointC":{"yAxis":225,"xAxis":1537}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"illegalPark":{"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":219,"xAxis":1161},"pointA":{"yAxis":805,"xAxis":72},"pointB":{"yAxis":1044,"xAxis":1439},"pointC":{"yAxis":272,"xAxis":1553}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}},"fog":{"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}}
             * name : 布控1
             * id : 4
             * trafficConfig : {"isOpen":true,"areas":[],"interval":5,"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
             * direction : {"endPoint":{"yAxis":482,"xAxis":1068},"startPoint":{"yAxis":611,"xAxis":937}}
             */

            private EventConfigsBean eventConfigs;
            private String name;
            private int id;
            private TrafficConfigBean trafficConfig;
            private DirectionBean direction;

            public EventConfigsBean getEventConfigs() {
                return eventConfigs;
            }

            public void setEventConfigs(EventConfigsBean eventConfigs) {
                this.eventConfigs = eventConfigs;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public TrafficConfigBean getTrafficConfig() {
                return trafficConfig;
            }

            public void setTrafficConfig(TrafficConfigBean trafficConfig) {
                this.trafficConfig = trafficConfig;
            }

            public DirectionBean getDirection() {
                return direction;
            }

            public void setDirection(DirectionBean direction) {
                this.direction = direction;
            }

            public static class EventConfigsBean {
                /**
                 * throwThings : {"isOpen":true,"num":10,"areas":[{"pointD":{"yAxis":251,"xAxis":1096},"pointA":{"yAxis":752,"xAxis":128},"pointB":{"yAxis":911,"xAxis":1487},"pointC":{"yAxis":292,"xAxis":1552}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * workSpace : {"isOpen":true,"count":5,"areas":[{"pointD":{"yAxis":975,"xAxis":1345},"pointA":{"yAxis":825,"xAxis":88},"pointB":{"yAxis":339,"xAxis":999},"pointC":{"yAxis":368,"xAxis":1493}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * illegalDriving : {"onEmergency":{"num":5,"areas":[]},"isOpen":true,"retrograde":{"areas":[{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}],"pixel":200},"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * fire : {"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * video : {"isOpen":true,"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * crowed : {"isOpen":true,"areas":[{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":312,"xAxis":1012},"pointA":{"yAxis":771,"xAxis":185},"pointB":{"yAxis":948,"xAxis":537},"pointC":{"yAxis":352,"xAxis":1153}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":280,"xAxis":1293},"pointA":{"yAxis":965,"xAxis":599},"pointB":{"yAxis":991,"xAxis":1351},"pointC":{"yAxis":315,"xAxis":1509}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * peopleOrNoVehicles : {"isOpen":true,"noVehicles":{"num":5,"areas":[{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]},"people":{"num":10,"areas":[{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]},"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * forbidCar : {"isOpen":true,"areas":[{"pointD":{"yAxis":197,"xAxis":1232},"pointA":{"yAxis":617,"xAxis":451},"pointB":{"yAxis":747,"xAxis":1421},"pointC":{"yAxis":225,"xAxis":1537}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * illegalPark : {"isOpen":true,"num":5,"areas":[{"pointD":{"yAxis":219,"xAxis":1161},"pointA":{"yAxis":805,"xAxis":72},"pointB":{"yAxis":1044,"xAxis":1439},"pointC":{"yAxis":272,"xAxis":1553}}],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 * fog : {"isOpen":true,"areas":[],"serviceTime":{"startTime":"00:00","endTime":"23:59"}}
                 */

                private ThrowThingsBean throwThings;
                private WorkSpaceBean workSpace;
                private IllegalDrivingBean illegalDriving;
                private FireBean fire;
                private VideoBean video;
                private CrowedBean crowed;
                private PeopleOrNoVehiclesBean peopleOrNoVehicles;
                private ForbidCarBean forbidCar;
                private IllegalParkBean illegalPark;
                private FogBean fog;

                public ThrowThingsBean getThrowThings() {
                    return throwThings;
                }

                public void setThrowThings(ThrowThingsBean throwThings) {
                    this.throwThings = throwThings;
                }

                public WorkSpaceBean getWorkSpace() {
                    return workSpace;
                }

                public void setWorkSpace(WorkSpaceBean workSpace) {
                    this.workSpace = workSpace;
                }

                public IllegalDrivingBean getIllegalDriving() {
                    return illegalDriving;
                }

                public void setIllegalDriving(IllegalDrivingBean illegalDriving) {
                    this.illegalDriving = illegalDriving;
                }

                public FireBean getFire() {
                    return fire;
                }

                public void setFire(FireBean fire) {
                    this.fire = fire;
                }

                public VideoBean getVideo() {
                    return video;
                }

                public void setVideo(VideoBean video) {
                    this.video = video;
                }

                public CrowedBean getCrowed() {
                    return crowed;
                }

                public void setCrowed(CrowedBean crowed) {
                    this.crowed = crowed;
                }

                public PeopleOrNoVehiclesBean getPeopleOrNoVehicles() {
                    return peopleOrNoVehicles;
                }

                public void setPeopleOrNoVehicles(PeopleOrNoVehiclesBean peopleOrNoVehicles) {
                    this.peopleOrNoVehicles = peopleOrNoVehicles;
                }

                public ForbidCarBean getForbidCar() {
                    return forbidCar;
                }

                public void setForbidCar(ForbidCarBean forbidCar) {
                    this.forbidCar = forbidCar;
                }

                public IllegalParkBean getIllegalPark() {
                    return illegalPark;
                }

                public void setIllegalPark(IllegalParkBean illegalPark) {
                    this.illegalPark = illegalPark;
                }

                public FogBean getFog() {
                    return fog;
                }

                public void setFog(FogBean fog) {
                    this.fog = fog;
                }

                public static class ThrowThingsBean {
                    /**
                     * isOpen : true
                     * num : 10
                     * areas : [{"pointD":{"yAxis":251,"xAxis":1096},"pointA":{"yAxis":752,"xAxis":128},"pointB":{"yAxis":911,"xAxis":1487},"pointC":{"yAxis":292,"xAxis":1552}}]
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private int num;
                    private ServiceTimeBean serviceTime;
                    private List<AreasBean> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public int getNum() {
                        return num;
                    }

                    public void setNum(int num) {
                        this.num = num;
                    }

                    public ServiceTimeBean getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBean serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<AreasBean> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<AreasBean> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBean {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }

                    public static class AreasBean {
                        /**
                         * pointD : {"yAxis":251,"xAxis":1096}
                         * pointA : {"yAxis":752,"xAxis":128}
                         * pointB : {"yAxis":911,"xAxis":1487}
                         * pointC : {"yAxis":292,"xAxis":1552}
                         */

                        private PointDBean pointD;
                        private PointABean pointA;
                        private PointBBean pointB;
                        private PointCBean pointC;

                        public PointDBean getPointD() {
                            return pointD;
                        }

                        public void setPointD(PointDBean pointD) {
                            this.pointD = pointD;
                        }

                        public PointABean getPointA() {
                            return pointA;
                        }

                        public void setPointA(PointABean pointA) {
                            this.pointA = pointA;
                        }

                        public PointBBean getPointB() {
                            return pointB;
                        }

                        public void setPointB(PointBBean pointB) {
                            this.pointB = pointB;
                        }

                        public PointCBean getPointC() {
                            return pointC;
                        }

                        public void setPointC(PointCBean pointC) {
                            this.pointC = pointC;
                        }

                        public static class PointDBean {
                            /**
                             * yAxis : 251
                             * xAxis : 1096
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointABean {
                            /**
                             * yAxis : 752
                             * xAxis : 128
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointBBean {
                            /**
                             * yAxis : 911
                             * xAxis : 1487
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointCBean {
                            /**
                             * yAxis : 292
                             * xAxis : 1552
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }
                    }
                }

                public static class WorkSpaceBean {
                    /**
                     * isOpen : true
                     * count : 5
                     * areas : [{"pointD":{"yAxis":975,"xAxis":1345},"pointA":{"yAxis":825,"xAxis":88},"pointB":{"yAxis":339,"xAxis":999},"pointC":{"yAxis":368,"xAxis":1493}}]
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private int count;
                    private ServiceTimeBeanX serviceTime;
                    private List<AreasBeanX> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public int getCount() {
                        return count;
                    }

                    public void setCount(int count) {
                        this.count = count;
                    }

                    public ServiceTimeBeanX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<AreasBeanX> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<AreasBeanX> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }

                    public static class AreasBeanX {
                        /**
                         * pointD : {"yAxis":975,"xAxis":1345}
                         * pointA : {"yAxis":825,"xAxis":88}
                         * pointB : {"yAxis":339,"xAxis":999}
                         * pointC : {"yAxis":368,"xAxis":1493}
                         */

                        private PointDBeanX pointD;
                        private PointABeanX pointA;
                        private PointBBeanX pointB;
                        private PointCBeanX pointC;

                        public PointDBeanX getPointD() {
                            return pointD;
                        }

                        public void setPointD(PointDBeanX pointD) {
                            this.pointD = pointD;
                        }

                        public PointABeanX getPointA() {
                            return pointA;
                        }

                        public void setPointA(PointABeanX pointA) {
                            this.pointA = pointA;
                        }

                        public PointBBeanX getPointB() {
                            return pointB;
                        }

                        public void setPointB(PointBBeanX pointB) {
                            this.pointB = pointB;
                        }

                        public PointCBeanX getPointC() {
                            return pointC;
                        }

                        public void setPointC(PointCBeanX pointC) {
                            this.pointC = pointC;
                        }

                        public static class PointDBeanX {
                            /**
                             * yAxis : 975
                             * xAxis : 1345
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointABeanX {
                            /**
                             * yAxis : 825
                             * xAxis : 88
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointBBeanX {
                            /**
                             * yAxis : 339
                             * xAxis : 999
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointCBeanX {
                            /**
                             * yAxis : 368
                             * xAxis : 1493
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }
                    }
                }

                public static class IllegalDrivingBean {
                    /**
                     * onEmergency : {"num":5,"areas":[]}
                     * isOpen : true
                     * retrograde : {"areas":[{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}],"pixel":200}
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private OnEmergencyBean onEmergency;
                    private boolean isOpen;
                    private RetrogradeBean retrograde;
                    private ServiceTimeBeanXX serviceTime;

                    public OnEmergencyBean getOnEmergency() {
                        return onEmergency;
                    }

                    public void setOnEmergency(OnEmergencyBean onEmergency) {
                        this.onEmergency = onEmergency;
                    }

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public RetrogradeBean getRetrograde() {
                        return retrograde;
                    }

                    public void setRetrograde(RetrogradeBean retrograde) {
                        this.retrograde = retrograde;
                    }

                    public ServiceTimeBeanXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public static class OnEmergencyBean {
                        /**
                         * num : 5
                         * areas : []
                         */

                        private int num;
                        private List<?> areas;

                        public int getNum() {
                            return num;
                        }

                        public void setNum(int num) {
                            this.num = num;
                        }

                        public List<?> getAreas() {
                            return areas;
                        }

                        public void setAreas(List<?> areas) {
                            this.areas = areas;
                        }
                    }

                    public static class RetrogradeBean {
                        /**
                         * areas : [{"pointD":{"yAxis":199,"xAxis":1233},"pointA":{"yAxis":686,"xAxis":333},"pointB":{"yAxis":807,"xAxis":1397},"pointC":{"yAxis":213,"xAxis":1540}}]
                         * pixel : 200
                         */

                        private int pixel;
                        private List<AreasBeanXX> areas;

                        public int getPixel() {
                            return pixel;
                        }

                        public void setPixel(int pixel) {
                            this.pixel = pixel;
                        }

                        public List<AreasBeanXX> getAreas() {
                            return areas;
                        }

                        public void setAreas(List<AreasBeanXX> areas) {
                            this.areas = areas;
                        }

                        public static class AreasBeanXX {
                            /**
                             * pointD : {"yAxis":199,"xAxis":1233}
                             * pointA : {"yAxis":686,"xAxis":333}
                             * pointB : {"yAxis":807,"xAxis":1397}
                             * pointC : {"yAxis":213,"xAxis":1540}
                             */

                            private PointDBeanXX pointD;
                            private PointABeanXX pointA;
                            private PointBBeanXX pointB;
                            private PointCBeanXX pointC;

                            public PointDBeanXX getPointD() {
                                return pointD;
                            }

                            public void setPointD(PointDBeanXX pointD) {
                                this.pointD = pointD;
                            }

                            public PointABeanXX getPointA() {
                                return pointA;
                            }

                            public void setPointA(PointABeanXX pointA) {
                                this.pointA = pointA;
                            }

                            public PointBBeanXX getPointB() {
                                return pointB;
                            }

                            public void setPointB(PointBBeanXX pointB) {
                                this.pointB = pointB;
                            }

                            public PointCBeanXX getPointC() {
                                return pointC;
                            }

                            public void setPointC(PointCBeanXX pointC) {
                                this.pointC = pointC;
                            }

                            public static class PointDBeanXX {
                                /**
                                 * yAxis : 199
                                 * xAxis : 1233
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointABeanXX {
                                /**
                                 * yAxis : 686
                                 * xAxis : 333
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointBBeanXX {
                                /**
                                 * yAxis : 807
                                 * xAxis : 1397
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointCBeanXX {
                                /**
                                 * yAxis : 213
                                 * xAxis : 1540
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }
                        }
                    }

                    public static class ServiceTimeBeanXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }
                }

                public static class FireBean {
                    /**
                     * isOpen : true
                     * areas : []
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private ServiceTimeBeanXXX serviceTime;
                    private List<?> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public ServiceTimeBeanXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<?> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<?> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }
                }

                public static class VideoBean {
                    /**
                     * isOpen : true
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private ServiceTimeBeanXXXX serviceTime;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public ServiceTimeBeanXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public static class ServiceTimeBeanXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }
                }

                public static class CrowedBean {
                    /**
                     * isOpen : true
                     * areas : [{"distance":40,"crowedName":"车道1","count":3,"pointD":{"yAxis":312,"xAxis":1012},"pointA":{"yAxis":771,"xAxis":185},"pointB":{"yAxis":948,"xAxis":537},"pointC":{"yAxis":352,"xAxis":1153}},{"distance":40,"crowedName":"车道2","count":3,"pointD":{"yAxis":280,"xAxis":1293},"pointA":{"yAxis":965,"xAxis":599},"pointB":{"yAxis":991,"xAxis":1351},"pointC":{"yAxis":315,"xAxis":1509}}]
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private ServiceTimeBeanXXXXX serviceTime;
                    private List<AreasBeanXXX> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public ServiceTimeBeanXXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<AreasBeanXXX> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<AreasBeanXXX> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanXXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }

                    public static class AreasBeanXXX {
                        /**
                         * distance : 40
                         * crowedName : 车道1
                         * count : 3
                         * pointD : {"yAxis":312,"xAxis":1012}
                         * pointA : {"yAxis":771,"xAxis":185}
                         * pointB : {"yAxis":948,"xAxis":537}
                         * pointC : {"yAxis":352,"xAxis":1153}
                         */

                        private int distance;
                        private String crowedName;
                        private int count;
                        private PointDBeanXXX pointD;
                        private PointABeanXXX pointA;
                        private PointBBeanXXX pointB;
                        private PointCBeanXXX pointC;

                        public int getDistance() {
                            return distance;
                        }

                        public void setDistance(int distance) {
                            this.distance = distance;
                        }

                        public String getCrowedName() {
                            return crowedName;
                        }

                        public void setCrowedName(String crowedName) {
                            this.crowedName = crowedName;
                        }

                        public int getCount() {
                            return count;
                        }

                        public void setCount(int count) {
                            this.count = count;
                        }

                        public PointDBeanXXX getPointD() {
                            return pointD;
                        }

                        public void setPointD(PointDBeanXXX pointD) {
                            this.pointD = pointD;
                        }

                        public PointABeanXXX getPointA() {
                            return pointA;
                        }

                        public void setPointA(PointABeanXXX pointA) {
                            this.pointA = pointA;
                        }

                        public PointBBeanXXX getPointB() {
                            return pointB;
                        }

                        public void setPointB(PointBBeanXXX pointB) {
                            this.pointB = pointB;
                        }

                        public PointCBeanXXX getPointC() {
                            return pointC;
                        }

                        public void setPointC(PointCBeanXXX pointC) {
                            this.pointC = pointC;
                        }

                        public static class PointDBeanXXX {
                            /**
                             * yAxis : 312
                             * xAxis : 1012
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointABeanXXX {
                            /**
                             * yAxis : 771
                             * xAxis : 185
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointBBeanXXX {
                            /**
                             * yAxis : 948
                             * xAxis : 537
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointCBeanXXX {
                            /**
                             * yAxis : 352
                             * xAxis : 1153
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }
                    }
                }

                public static class PeopleOrNoVehiclesBean {
                    /**
                     * isOpen : true
                     * noVehicles : {"num":5,"areas":[{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]}
                     * people : {"num":10,"areas":[{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]}
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private NoVehiclesBean noVehicles;
                    private PeopleBean people;
                    private ServiceTimeBeanXXXXXX serviceTime;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public NoVehiclesBean getNoVehicles() {
                        return noVehicles;
                    }

                    public void setNoVehicles(NoVehiclesBean noVehicles) {
                        this.noVehicles = noVehicles;
                    }

                    public PeopleBean getPeople() {
                        return people;
                    }

                    public void setPeople(PeopleBean people) {
                        this.people = people;
                    }

                    public ServiceTimeBeanXXXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public static class NoVehiclesBean {
                        /**
                         * num : 5
                         * areas : [{"pointD":{"yAxis":239,"xAxis":1115},"pointA":{"yAxis":715,"xAxis":201},"pointB":{"yAxis":885,"xAxis":1492},"pointC":{"yAxis":289,"xAxis":1555}}]
                         */

                        private int num;
                        private List<AreasBeanXXXX> areas;

                        public int getNum() {
                            return num;
                        }

                        public void setNum(int num) {
                            this.num = num;
                        }

                        public List<AreasBeanXXXX> getAreas() {
                            return areas;
                        }

                        public void setAreas(List<AreasBeanXXXX> areas) {
                            this.areas = areas;
                        }

                        public static class AreasBeanXXXX {
                            /**
                             * pointD : {"yAxis":239,"xAxis":1115}
                             * pointA : {"yAxis":715,"xAxis":201}
                             * pointB : {"yAxis":885,"xAxis":1492}
                             * pointC : {"yAxis":289,"xAxis":1555}
                             */

                            private PointDBeanXXXX pointD;
                            private PointABeanXXXX pointA;
                            private PointBBeanXXXX pointB;
                            private PointCBeanXXXX pointC;

                            public PointDBeanXXXX getPointD() {
                                return pointD;
                            }

                            public void setPointD(PointDBeanXXXX pointD) {
                                this.pointD = pointD;
                            }

                            public PointABeanXXXX getPointA() {
                                return pointA;
                            }

                            public void setPointA(PointABeanXXXX pointA) {
                                this.pointA = pointA;
                            }

                            public PointBBeanXXXX getPointB() {
                                return pointB;
                            }

                            public void setPointB(PointBBeanXXXX pointB) {
                                this.pointB = pointB;
                            }

                            public PointCBeanXXXX getPointC() {
                                return pointC;
                            }

                            public void setPointC(PointCBeanXXXX pointC) {
                                this.pointC = pointC;
                            }

                            public static class PointDBeanXXXX {
                                /**
                                 * yAxis : 239
                                 * xAxis : 1115
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointABeanXXXX {
                                /**
                                 * yAxis : 715
                                 * xAxis : 201
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointBBeanXXXX {
                                /**
                                 * yAxis : 885
                                 * xAxis : 1492
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointCBeanXXXX {
                                /**
                                 * yAxis : 289
                                 * xAxis : 1555
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }
                        }
                    }

                    public static class PeopleBean {
                        /**
                         * num : 10
                         * areas : [{"pointD":{"yAxis":247,"xAxis":1100},"pointA":{"yAxis":702,"xAxis":227},"pointB":{"yAxis":933,"xAxis":1488},"pointC":{"yAxis":285,"xAxis":1557}}]
                         */

                        private int num;
                        private List<AreasBeanXXXXX> areas;

                        public int getNum() {
                            return num;
                        }

                        public void setNum(int num) {
                            this.num = num;
                        }

                        public List<AreasBeanXXXXX> getAreas() {
                            return areas;
                        }

                        public void setAreas(List<AreasBeanXXXXX> areas) {
                            this.areas = areas;
                        }

                        public static class AreasBeanXXXXX {
                            /**
                             * pointD : {"yAxis":247,"xAxis":1100}
                             * pointA : {"yAxis":702,"xAxis":227}
                             * pointB : {"yAxis":933,"xAxis":1488}
                             * pointC : {"yAxis":285,"xAxis":1557}
                             */

                            private PointDBeanXXXXX pointD;
                            private PointABeanXXXXX pointA;
                            private PointBBeanXXXXX pointB;
                            private PointCBeanXXXXX pointC;

                            public PointDBeanXXXXX getPointD() {
                                return pointD;
                            }

                            public void setPointD(PointDBeanXXXXX pointD) {
                                this.pointD = pointD;
                            }

                            public PointABeanXXXXX getPointA() {
                                return pointA;
                            }

                            public void setPointA(PointABeanXXXXX pointA) {
                                this.pointA = pointA;
                            }

                            public PointBBeanXXXXX getPointB() {
                                return pointB;
                            }

                            public void setPointB(PointBBeanXXXXX pointB) {
                                this.pointB = pointB;
                            }

                            public PointCBeanXXXXX getPointC() {
                                return pointC;
                            }

                            public void setPointC(PointCBeanXXXXX pointC) {
                                this.pointC = pointC;
                            }

                            public static class PointDBeanXXXXX {
                                /**
                                 * yAxis : 247
                                 * xAxis : 1100
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointABeanXXXXX {
                                /**
                                 * yAxis : 702
                                 * xAxis : 227
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointBBeanXXXXX {
                                /**
                                 * yAxis : 933
                                 * xAxis : 1488
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }

                            public static class PointCBeanXXXXX {
                                /**
                                 * yAxis : 285
                                 * xAxis : 1557
                                 */

                                private int yAxis;
                                private int xAxis;

                                public int getYAxis() {
                                    return yAxis;
                                }

                                public void setYAxis(int yAxis) {
                                    this.yAxis = yAxis;
                                }

                                public int getXAxis() {
                                    return xAxis;
                                }

                                public void setXAxis(int xAxis) {
                                    this.xAxis = xAxis;
                                }
                            }
                        }
                    }

                    public static class ServiceTimeBeanXXXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }
                }

                public static class ForbidCarBean {
                    /**
                     * isOpen : true
                     * areas : [{"pointD":{"yAxis":197,"xAxis":1232},"pointA":{"yAxis":617,"xAxis":451},"pointB":{"yAxis":747,"xAxis":1421},"pointC":{"yAxis":225,"xAxis":1537}}]
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private ServiceTimeBeanXXXXXXX serviceTime;
                    private List<AreasBeanXXXXXX> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public ServiceTimeBeanXXXXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<AreasBeanXXXXXX> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<AreasBeanXXXXXX> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanXXXXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }

                    public static class AreasBeanXXXXXX {
                        /**
                         * pointD : {"yAxis":197,"xAxis":1232}
                         * pointA : {"yAxis":617,"xAxis":451}
                         * pointB : {"yAxis":747,"xAxis":1421}
                         * pointC : {"yAxis":225,"xAxis":1537}
                         */

                        private PointDBeanXXXXXX pointD;
                        private PointABeanXXXXXX pointA;
                        private PointBBeanXXXXXX pointB;
                        private PointCBeanXXXXXX pointC;

                        public PointDBeanXXXXXX getPointD() {
                            return pointD;
                        }

                        public void setPointD(PointDBeanXXXXXX pointD) {
                            this.pointD = pointD;
                        }

                        public PointABeanXXXXXX getPointA() {
                            return pointA;
                        }

                        public void setPointA(PointABeanXXXXXX pointA) {
                            this.pointA = pointA;
                        }

                        public PointBBeanXXXXXX getPointB() {
                            return pointB;
                        }

                        public void setPointB(PointBBeanXXXXXX pointB) {
                            this.pointB = pointB;
                        }

                        public PointCBeanXXXXXX getPointC() {
                            return pointC;
                        }

                        public void setPointC(PointCBeanXXXXXX pointC) {
                            this.pointC = pointC;
                        }

                        public static class PointDBeanXXXXXX {
                            /**
                             * yAxis : 197
                             * xAxis : 1232
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointABeanXXXXXX {
                            /**
                             * yAxis : 617
                             * xAxis : 451
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointBBeanXXXXXX {
                            /**
                             * yAxis : 747
                             * xAxis : 1421
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointCBeanXXXXXX {
                            /**
                             * yAxis : 225
                             * xAxis : 1537
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }
                    }
                }

                public static class IllegalParkBean {
                    /**
                     * isOpen : true
                     * num : 5
                     * areas : [{"pointD":{"yAxis":219,"xAxis":1161},"pointA":{"yAxis":805,"xAxis":72},"pointB":{"yAxis":1044,"xAxis":1439},"pointC":{"yAxis":272,"xAxis":1553}}]
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private int num;
                    private ServiceTimeBeanXXXXXXXX serviceTime;
                    private List<AreasBeanXXXXXXX> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public int getNum() {
                        return num;
                    }

                    public void setNum(int num) {
                        this.num = num;
                    }

                    public ServiceTimeBeanXXXXXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXXXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<AreasBeanXXXXXXX> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<AreasBeanXXXXXXX> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanXXXXXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }

                    public static class AreasBeanXXXXXXX {
                        /**
                         * pointD : {"yAxis":219,"xAxis":1161}
                         * pointA : {"yAxis":805,"xAxis":72}
                         * pointB : {"yAxis":1044,"xAxis":1439}
                         * pointC : {"yAxis":272,"xAxis":1553}
                         */

                        private PointDBeanXXXXXXX pointD;
                        private PointABeanXXXXXXX pointA;
                        private PointBBeanXXXXXXX pointB;
                        private PointCBeanXXXXXXX pointC;

                        public PointDBeanXXXXXXX getPointD() {
                            return pointD;
                        }

                        public void setPointD(PointDBeanXXXXXXX pointD) {
                            this.pointD = pointD;
                        }

                        public PointABeanXXXXXXX getPointA() {
                            return pointA;
                        }

                        public void setPointA(PointABeanXXXXXXX pointA) {
                            this.pointA = pointA;
                        }

                        public PointBBeanXXXXXXX getPointB() {
                            return pointB;
                        }

                        public void setPointB(PointBBeanXXXXXXX pointB) {
                            this.pointB = pointB;
                        }

                        public PointCBeanXXXXXXX getPointC() {
                            return pointC;
                        }

                        public void setPointC(PointCBeanXXXXXXX pointC) {
                            this.pointC = pointC;
                        }

                        public static class PointDBeanXXXXXXX {
                            /**
                             * yAxis : 219
                             * xAxis : 1161
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointABeanXXXXXXX {
                            /**
                             * yAxis : 805
                             * xAxis : 72
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointBBeanXXXXXXX {
                            /**
                             * yAxis : 1044
                             * xAxis : 1439
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }

                        public static class PointCBeanXXXXXXX {
                            /**
                             * yAxis : 272
                             * xAxis : 1553
                             */

                            private int yAxis;
                            private int xAxis;

                            public int getYAxis() {
                                return yAxis;
                            }

                            public void setYAxis(int yAxis) {
                                this.yAxis = yAxis;
                            }

                            public int getXAxis() {
                                return xAxis;
                            }

                            public void setXAxis(int xAxis) {
                                this.xAxis = xAxis;
                            }
                        }
                    }
                }

                public static class FogBean {
                    /**
                     * isOpen : true
                     * areas : []
                     * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                     */

                    private boolean isOpen;
                    private ServiceTimeBeanXXXXXXXXX serviceTime;
                    private List<?> areas;

                    public boolean isIsOpen() {
                        return isOpen;
                    }

                    public void setIsOpen(boolean isOpen) {
                        this.isOpen = isOpen;
                    }

                    public ServiceTimeBeanXXXXXXXXX getServiceTime() {
                        return serviceTime;
                    }

                    public void setServiceTime(ServiceTimeBeanXXXXXXXXX serviceTime) {
                        this.serviceTime = serviceTime;
                    }

                    public List<?> getAreas() {
                        return areas;
                    }

                    public void setAreas(List<?> areas) {
                        this.areas = areas;
                    }

                    public static class ServiceTimeBeanXXXXXXXXX {
                        /**
                         * startTime : 00:00
                         * endTime : 23:59
                         */

                        private String startTime;
                        private String endTime;

                        public String getStartTime() {
                            return startTime;
                        }

                        public void setStartTime(String startTime) {
                            this.startTime = startTime;
                        }

                        public String getEndTime() {
                            return endTime;
                        }

                        public void setEndTime(String endTime) {
                            this.endTime = endTime;
                        }
                    }
                }
            }

            public static class TrafficConfigBean {
                /**
                 * isOpen : true
                 * areas : []
                 * interval : 5
                 * serviceTime : {"startTime":"00:00","endTime":"23:59"}
                 */

                private boolean isOpen;
                private int interval;
                private ServiceTimeBeanXXXXXXXXXX serviceTime;
                private List<?> areas;

                public boolean isIsOpen() {
                    return isOpen;
                }

                public void setIsOpen(boolean isOpen) {
                    this.isOpen = isOpen;
                }

                public int getInterval() {
                    return interval;
                }

                public void setInterval(int interval) {
                    this.interval = interval;
                }

                public ServiceTimeBeanXXXXXXXXXX getServiceTime() {
                    return serviceTime;
                }

                public void setServiceTime(ServiceTimeBeanXXXXXXXXXX serviceTime) {
                    this.serviceTime = serviceTime;
                }

                public List<?> getAreas() {
                    return areas;
                }

                public void setAreas(List<?> areas) {
                    this.areas = areas;
                }

                public static class ServiceTimeBeanXXXXXXXXXX {
                    /**
                     * startTime : 00:00
                     * endTime : 23:59
                     */

                    private String startTime;
                    private String endTime;

                    public String getStartTime() {
                        return startTime;
                    }

                    public void setStartTime(String startTime) {
                        this.startTime = startTime;
                    }

                    public String getEndTime() {
                        return endTime;
                    }

                    public void setEndTime(String endTime) {
                        this.endTime = endTime;
                    }
                }
            }

            public static class DirectionBean {
                /**
                 * endPoint : {"yAxis":482,"xAxis":1068}
                 * startPoint : {"yAxis":611,"xAxis":937}
                 */

                private EndPointBean endPoint;
                private StartPointBean startPoint;

                public EndPointBean getEndPoint() {
                    return endPoint;
                }

                public void setEndPoint(EndPointBean endPoint) {
                    this.endPoint = endPoint;
                }

                public StartPointBean getStartPoint() {
                    return startPoint;
                }

                public void setStartPoint(StartPointBean startPoint) {
                    this.startPoint = startPoint;
                }

                public static class EndPointBean {
                    /**
                     * yAxis : 482
                     * xAxis : 1068
                     */

                    private int yAxis;
                    private int xAxis;

                    public int getYAxis() {
                        return yAxis;
                    }

                    public void setYAxis(int yAxis) {
                        this.yAxis = yAxis;
                    }

                    public int getXAxis() {
                        return xAxis;
                    }

                    public void setXAxis(int xAxis) {
                        this.xAxis = xAxis;
                    }
                }

                public static class StartPointBean {
                    /**
                     * yAxis : 611
                     * xAxis : 937
                     */

                    private int yAxis;
                    private int xAxis;

                    public int getYAxis() {
                        return yAxis;
                    }

                    public void setYAxis(int yAxis) {
                        this.yAxis = yAxis;
                    }

                    public int getXAxis() {
                        return xAxis;
                    }

                    public void setXAxis(int xAxis) {
                        this.xAxis = xAxis;
                    }
                }
            }
        }
    }
}
