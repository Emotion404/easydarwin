package com.qsj.easydarwin.service.impl;

import com.qsj.easydarwin.pojo.CameraConfig;
import com.qsj.easydarwin.pojo.CameraConfigListResponse;
import com.qsj.easydarwin.pojo.CameraConfigVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import com.qsj.easydarwin.dao.CameraConfigMapper;
import com.qsj.easydarwin.service.CameraConfigService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CameraConfigServiceImpl implements CameraConfigService {

    @Resource
    private CameraConfigMapper cameraConfigMapper;


    @Override
    public List<CameraConfigListResponse> list(String rtsp) {
        CameraConfig config = new CameraConfig();
        config.setRtsp(rtsp);
        List<CameraConfig> select = this.cameraConfigMapper.select(config);
        List<CameraConfigListResponse> cameraConfigList = new ArrayList<>();
        for (CameraConfig cameraConfig : select) {
            cameraConfigList.add(new CameraConfigListResponse(cameraConfig.getId(), cameraConfig.getConfigName()));
        }
        return cameraConfigList;
    }

    @Override
    public CameraConfig findById(int id) {
        return this.cameraConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public int save(CameraConfigVO request) {
        // vo->po
        System.out.println("====");
        CameraConfig cameraConfig = this.voToPo(request);
        System.out.println(cameraConfig);
        if (cameraConfig.getId() == null) {
            return this.cameraConfigMapper.insertSelective(cameraConfig);
        } else {
            return this.cameraConfigMapper.updateByPrimaryKeySelective(cameraConfig);
        }
    }


    private CameraConfig voToPo(CameraConfigVO vo) {
        CameraConfig po = new CameraConfig();
        BeanUtils.copyProperties(vo, po);
        po.setTrafficFlowStart(vo.getTrafficFlowTime().substring(0, 8));
        po.setTrafficFlowEnd(vo.getTrafficFlowTime().substring(11));
        po.setParkStart(vo.getParkTime().substring(0, 8));
        po.setParkEnd(vo.getParkTime().substring(11));
        po.setProhibitedVehiclesStart(vo.getProhibitedVehiclesTime().substring(0, 8));
        po.setProhibitedVehiclesEnd(vo.getProhibitedVehiclesTime().substring(11));
        po.setIllegalDrivingStart(vo.getIllegalDrivingTime().substring(0, 8));
        po.setIllegalDrivingEnd(vo.getIllegalDrivingTime().substring(11));
        po.setPeopleStart(vo.getPeopleTime().substring(0, 8));
        po.setPeopleEnd(vo.getPeopleTime().substring(11));
        po.setThrowingStart(vo.getThrowingTime().substring(0, 8));
        po.setThrowingEnd(vo.getThrowingTime().substring(11));
        po.setTrafficJamStart(vo.getTrafficJamTime().substring(0, 8));
        po.setTrafficJamEnd(vo.getTrafficFlowTime().substring(11));
        po.setConstructionStart(vo.getConstructionTime().substring(0, 8));
        po.setConstructionEnd(vo.getConstructionTime().substring(11));
        po.setFireStart(vo.getFireTime().substring(0, 8));
        po.setFireEnd(vo.getFireTime().substring(11));
        po.setFogStart(vo.getFogTime().substring(0, 8));
        po.setFogEnd(vo.getFogTime().substring(11));
        po.setVideoQualityStart(vo.getVideoQualityTime().substring(0, 8));
        po.setVideoQualityEnd(vo.getVideoQualityTime().substring(11));
        return po;
    }

}
