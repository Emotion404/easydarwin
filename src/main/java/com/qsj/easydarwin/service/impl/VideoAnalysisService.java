package com.qsj.easydarwin.service.impl;

import com.qsj.easydarwin.dao.VideoAnalysisMapper;
import com.qsj.easydarwin.pojo.SaveVideoAnalysisVO;
import com.qsj.easydarwin.pojo.VideoAnalysis;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
public class VideoAnalysisService {

    @Resource
    private VideoAnalysisMapper videoAnalysisMapper;

    public int insert(SaveVideoAnalysisVO request) {
        VideoAnalysis videoAnalysis = new VideoAnalysis();
        BeanUtils.copyProperties(request, videoAnalysis);
        // list->string   用“、”分割
        videoAnalysis.setEvent(request.getEvent() == null ? null : String.join("、",request.getEvent()));
        videoAnalysis.setGoods(request.getGoods() == null ? null : String.join("、",request.getGoods()));

        return this.videoAnalysisMapper.insertSelective(videoAnalysis);
    }
    public List<VideoAnalysis> selectAll() {
        return this.videoAnalysisMapper.selectAll();
    }

    public SaveVideoAnalysisVO getByVideoName(String videoName) {
        VideoAnalysis example = new VideoAnalysis();
        example.setVideoName(videoName);
        List<VideoAnalysis> select = this.videoAnalysisMapper.select(example);
        if (select.size()>0){
            VideoAnalysis videoAnalysis = select.get(0);
            SaveVideoAnalysisVO response = new SaveVideoAnalysisVO();
            BeanUtils.copyProperties(videoAnalysis,response);
            String[] goodsArr = videoAnalysis.getGoods().split("、");
            String[] eventsArr = videoAnalysis.getEvent().split("、");
            response.setGoods(Arrays.asList(goodsArr));
            response.setEvent(Arrays.asList(eventsArr));
            return response;
        }else {
            return null;
        }
    }



    public int deleteByVideoName(String videoName) {
        VideoAnalysis example = new VideoAnalysis();
        example.setVideoName(videoName);
        return this.videoAnalysisMapper.delete(example);
    }
}
