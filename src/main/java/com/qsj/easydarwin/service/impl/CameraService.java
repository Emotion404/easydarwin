package com.qsj.easydarwin.service.impl;

import com.qsj.easydarwin.config.Config;
import com.qsj.easydarwin.dao.CameraDao;
import com.qsj.easydarwin.pojo.Camera;
import com.qsj.easydarwin.pojo.DropDownResponse;
import com.qsj.easydarwin.pojo.base.BasePageResponse;
import com.qsj.easydarwin.service.DoServiceBase;
import com.qsj.easydarwin.util.ImageUtil;
import com.qsj.easydarwin.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CameraService extends DoServiceBase {

    @Autowired
    private Config config;

    @Autowired
    private CameraDao cameraDao;

    /**
     * 根据视频URL 新增点位
     */
    public boolean add(Camera camera) {
        // 删除原有
        cameraDao.delete(camera.getId());
        String base64Img = camera.getCameraDetails().getSceneImg();
//        camera.getCameraDetails().setSceneImg("");
//        camera.getCameraDetails().setSceneImgUrl(config.getDomain()+"resource/getcameraimage/"+camera.getId());
        //点位保存
        boolean result = cameraDao.add(camera);
        if (!result)
            return false;

        //图片保存到本地
//        result = uploadImage(camera.getId(), base64Img);

        return result;
    }

    /**
     * 修改点位
     */
    public boolean update(Camera camera) {
        //保存
        boolean result = cameraDao.update(camera);
        return result;
    }

    /**
     * 删除点位
     */
    public boolean delete(String id) {
        Camera camera = get(id);
        if (camera == null)
            return false;
        //删除
        boolean result = cameraDao.delete(camera.getId());
        if (!result)
            return false;

        //删除照片
        if (camera.getCameraDetails().getSceneImg() == null || camera.getCameraDetails().getSceneImg().equals(""))
            return true;

        result = ImageUtil.deleteImage(camera.getCameraDetails().getSceneImg());
        return result;
    }

    /**
     * 获取一个点位
     */
    public Camera get(String id) {
        //获取一个点位
        Camera result = cameraDao.getById(id);
        return result;
    }

    /**
     * 获取所有点位
     */
    public List<Camera> list() {
        return cameraDao.getList();
    }

    /**
     * 列表
     */
    public BasePageResponse<Camera> list(String name, int page, int pageSize) {
        BasePageResponse<Camera> result = new BasePageResponse<Camera>();
        List<Camera> cameraList = list();
        if (cameraList == null || cameraList.size() == 0) {
            result.setCount(0);
            return result;
        }

        List<Camera> showList = new ArrayList<Camera>();

        for (int i = 0; i < cameraList.size(); i++) {
            //如果name不存在
            if (name == null || "".equals(name)) {
                showList.add(cameraList.get(i));
            } else {
                if (cameraList.get(i).getCameraDetails().getName().contains(name)) {
                    showList.add(cameraList.get(i));
                }
            }
        }
        Collections.reverse(showList); // 倒序排列

        result.setCount(showList.size());
        result.setList(PageUtil.page(showList, page, pageSize));

        return result;
    }

    public List<Camera> list(String name){
        List<Camera> cameraList = list();
        List<Camera> showList = new ArrayList<Camera>();

        for (int i = 0; i < cameraList.size(); i++) {
            //如果name不存在
            if (name == null || "".equals(name)) {
                showList.add(cameraList.get(i));
            } else {
                if (cameraList.get(i).getCameraDetails().getName().contains(name)) {
                    showList.add(cameraList.get(i));
                }
            }
        }
        Collections.reverse(showList); // 倒序排列
        return  showList;
    }

    /**
     * 点位图片保存
     */
    public boolean uploadImage(String cameraId, String imageBase64String) {
        Camera camera = get(cameraId);
        if (camera == null) {
            logger.warn("图片保存失败：点位不存在");
            return false;
        }

        String filePath = String.format("%s/cameraImage", config.getCameraImgSavePath());

        String fileName = String.format("%s.jpg", cameraId);
        if (imageBase64String.contains("png")) {
            fileName = String.format("%s.png", cameraId);
        }
        boolean result = ImageUtil.save(imageBase64String, filePath, fileName);
        if (!result)
            return false;

        String fileUrl = String.format("%s/%s", filePath, fileName);
        camera.getCameraDetails().setSceneImg(fileUrl);
        result = cameraDao.update(camera);

        return result;
    }

    /**
     * 点位下拉框
     */
    public List<DropDownResponse> dropDown() {
        List<DropDownResponse> result = new ArrayList<>();

        List<Camera> list = list();
        if (list == null || list.size() == 0)
            return result;

        for (Camera camera : list) {
            DropDownResponse response=new DropDownResponse();
            response.setLabel(camera.getCameraDetails().getName());
            response.setValue(camera.getId());
            result.add(response);
        }

        return result;
    }
}
