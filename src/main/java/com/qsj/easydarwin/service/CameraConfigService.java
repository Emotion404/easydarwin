package com.qsj.easydarwin.service;

import com.qsj.easydarwin.pojo.CameraConfig;
import com.qsj.easydarwin.pojo.CameraConfigListResponse;
import com.qsj.easydarwin.pojo.CameraConfigVO;

import java.util.List;

public interface CameraConfigService{
    /**
     * 根据rtsp地址查询配置
     * @param rtsp
     * @return
     */
    List<CameraConfigListResponse> list(String rtsp);

    CameraConfig findById(int id);

    int save(CameraConfigVO request);
}
