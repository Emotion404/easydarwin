package com.qsj.easydarwin.service;

import com.qsj.easydarwin.pojo.FileListResponse;

import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/16 17:31
 */
public interface FileService {
    List<FileListResponse> getFileList(String videoPath);
    List<FileListResponse> getDirList();

    String saveCurrentImage(String videoUrl, Long time);
}
