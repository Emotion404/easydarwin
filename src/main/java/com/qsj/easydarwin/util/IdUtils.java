package com.qsj.easydarwin.util;

import java.util.UUID;

/**
 * id生成器
 */
public class IdUtils {

    /**
     * 获取id
     */
    public static String getId() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "");
    }
}
