package com.qsj.easydarwin.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/24 16:08
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Response<T> {
    private boolean success;
    private T data;
}
