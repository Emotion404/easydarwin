package com.qsj.easydarwin.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/13 14:05
 */
@Component
@Getter
public class Config {
    @Value("${easydarwin.path}")
    private String serverPath;

    @Value("${video.path}")
    private String videoPath;
    @Value("${rtsp.path}")
    private String rtspPath;
    @Value("${image.path}")
    private String imagePath;
    /**
     * 点位文件路径
     */
    @Value("${com.config.cameraPath}")
    private String cameraPath;

    /**
     * 域名
     */
    @Value("${com.config.domain}")
    private String domain;

    /**
     * 点位场景照保存路径
     */
    @Value("${com.config.cameraImgSavePath}")
    private String cameraImgSavePath;

}
