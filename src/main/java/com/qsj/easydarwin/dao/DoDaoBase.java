package com.qsj.easydarwin.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据持久化
 */
public class DoDaoBase {
    protected static final Logger logger = LoggerFactory.getLogger(DoDaoBase.class);

    //读取文件所有内容
    public String readAll(String filePath) {
        BufferedReader br = null;
        StringBuilder result = new StringBuilder();
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                logger.warn(String.format("路径：%s不存在。", filePath));
                return null;
            }
            InputStreamReader isr=new InputStreamReader(new FileInputStream(file));
            br = new BufferedReader(isr);
            String s = null;
            while ((s = br.readLine()) != null) {
                result.append(System.lineSeparator() + s);
            }
        } catch (Exception e) {
            logger.error("文件读取异常", e);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                logger.error("文件流读取关闭异常", ex);
            }
        }
        return result.toString();
    }

    //按行读取文件内容
    public List<String> readAllByLine(String filePath) {
        List<String> result = new ArrayList();
        BufferedReader br = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                logger.warn(String.format("路径：%s不存在。", filePath));
                return null;
            }
            InputStreamReader isr=new InputStreamReader(new FileInputStream(file));
            br = new BufferedReader(isr);
            String s = null;
            while ((s = br.readLine()) != null) {
                result.add(s);
            }
        } catch (Exception e) {
            logger.error("文件读取异常", e);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                logger.error("文件流读取关闭异常", ex);
            }
        }
        return result;
    }

    //写入文件（覆盖原来内容）
    public void writeAll(String filePath, String content) {
        Writer writer = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                logger.warn(String.format("路径%s不存在。", filePath));
                return;
            }

            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            writer.write(content);
        } catch (Exception e) {
            logger.error("文件写入异常", e);
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException ex) {
                logger.error("文件流写入关闭异常", ex);
            }
        }
    }

    //写入文件（最后追加一行）
    public void writeLine(String filePath, String content) {
        BufferedWriter writer = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                logger.warn(String.format("路径：%s不存在。", filePath));
                return;
            }
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
            writer.write(content);
            writer.write(System.lineSeparator());//写入换行
        } catch (Exception e) {
            logger.error("文件写入异常", e);
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException ex) {
                logger.error("文件流写入关闭异常", ex);
            }
        }
    }
}
