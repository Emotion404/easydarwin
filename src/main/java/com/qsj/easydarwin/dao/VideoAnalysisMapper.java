package com.qsj.easydarwin.dao;

import com.qsj.easydarwin.pojo.VideoAnalysis;
import tk.mybatis.mapper.common.Mapper;

public interface VideoAnalysisMapper extends Mapper<VideoAnalysis> {
}