package com.qsj.easydarwin.dao;

import com.qsj.easydarwin.pojo.CameraConfig;
import tk.mybatis.mapper.common.Mapper;

public interface CameraConfigMapper extends Mapper<CameraConfig> {
}