package com.qsj.easydarwin.dao;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qsj.easydarwin.config.Config;
import com.qsj.easydarwin.pojo.Camera;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CameraDao extends DoDaoBase {

    @Autowired
    private Config config;

    /**
     * 获取列表
     */
    public List<Camera> getList() {
        String userJsonString = readAll(config.getCameraPath());
        List<Camera> userList = JSONObject.parseArray(userJsonString, Camera.class);
        return userList;
    }

    /**
     * 获取单个点位
     */
    public Camera getByName(String name) {
        List<Camera> cameraList = getList();
        if (cameraList == null || cameraList.size() == 0) {
            logger.warn("请先配置点位信息");
            return null;
        }
        for (Camera camera : cameraList) {
            if (name.equals(camera.getCameraDetails().getName())) {
                return camera;
            }
        }
        return null;
    }

    /**
     * 获取单个点位
     */
    public Camera getById(String id) {
        List<Camera> cameraList = getList();
        if (cameraList == null || cameraList.size() == 0) {
            logger.warn("请先配置点位信息");
            return null;
        }
        for (Camera camera : cameraList) {
            if (id.equals(camera.getId())) {
                return camera;
            }
        }
        return null;
    }

    /**
     * 删除
     */
    public boolean delete(String id) {
        List<Camera> cameraList = getList();
        if (cameraList == null || cameraList.size() == 0) {
            logger.warn("请先配置点位信息");
            return true;
        }

        int index = -1;
        for (int i = 0; i < cameraList.size(); i++) {
            if (cameraList.get(i).getId().equals(id)) {
                index = i;
                break;
            }
        }

        //删除
        if (index == -1) {
            return true;
        }

        //移除
        cameraList.remove(index);
        //保存
        writeAll(config.getCameraPath(), JSONArray.toJSONString(cameraList));

        return true;
    }

    /**
     * 新增
     */
    public boolean add(Camera camera) {
        List<Camera> cameraList = getList();
        if (cameraList == null) {
            cameraList = new ArrayList<Camera>();
        }

        //插入
        cameraList.add(camera);
        //保存
        writeAll(config.getCameraPath(), JSONArray.toJSONString(cameraList));

        return true;
    }

    /**
     * 修改
     */
    public boolean update(Camera camera) {
        List<Camera> cameraList = getList();
        if (cameraList == null || cameraList.size() == 0) {
            logger.warn("请先配置点位信息");
            return false;
        }

        int index = -1;
        for (int i = 0; i < cameraList.size(); i++) {
            if (camera.getId().equals(cameraList.get(i).getId())) {
                index = i;
                break;
            }
        }

        if (index == -1)
            return false;

        //更换
        cameraList.set(index, camera);
        //保存
        writeAll(config.getCameraPath(), JSONArray.toJSONString(cameraList));

        return true;
    }
}
