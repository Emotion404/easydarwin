package com.qsj.easydarwin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/9 15:59
 */
@Controller
public class IndexController {


    @GetMapping("/")
    public String index() {
        return "redirect:/file/list";
    }



    /// m3u8 直播
    /*@GetMapping("/video")
    public ModelAndView video(String url) {
//        rtsp://127.0.0.1/group1/illegalTraffic01.mp4
//        http://127.0.0.1:10008/record/group1//illegalTraffic01.mp4//20200713//out.m3u8
        System.out.println("url = " + url);
        StringBuilder m3u8 = new StringBuilder(config.getVideoApiPrefix());
        String[] split = url.split("/");
        int length = split.length;
        m3u8.append("/").append(split[length - 2])
                .append("/").append(split[length - 1])
                .append("/").append(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .append(config.getVideoApiSuffix());
        System.out.println(m3u8);
        ModelAndView mv = new ModelAndView("video");
        mv.addObject("videoUrl", m3u8.toString());
        return mv;
    }
*/
}
