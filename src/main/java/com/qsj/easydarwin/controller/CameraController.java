package com.qsj.easydarwin.controller;

import com.alibaba.fastjson.JSONObject;
import com.qsj.easydarwin.config.Config;
import com.qsj.easydarwin.pojo.*;
import com.qsj.easydarwin.pojo.base.Area;
import com.qsj.easydarwin.pojo.base.EventConfig.*;
import com.qsj.easydarwin.pojo.base.EventConfigs;
import com.qsj.easydarwin.pojo.base.Road;
import com.qsj.easydarwin.service.CameraConfigService;
import com.qsj.easydarwin.service.impl.CameraService;
import com.qsj.easydarwin.util.IdUtils;
import com.qsj.easydarwin.util.ResultUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/21 16:59
 */
@RestController
@RequestMapping("/camera")
public class CameraController {
    @Autowired
    private CameraConfigService cameraConfigService;
    @Autowired
    private CameraService cameraService;
    @Autowired
    private Config config;

/*    @GetMapping("/configList")
    public ModelAndView configList(String url) {
        ModelAndView mv = new ModelAndView("/camera-config-list");
//        System.out.println(url);
        String rtsp = config.getRtspPath()+url.substring(6);
//        System.out.println("rtsp = " + rtsp);
        List<CameraConfigListResponse> cameraConfigList = this.cameraConfigService.list(rtsp);
        mv.addObject("rtsp",rtsp);
        mv.addObject("cameraConfigList",cameraConfigList);
        return mv;
    }*/

    @PostMapping("/saveConfig")
    public String configSave(CameraConfigVO request) {
        System.out.println(request);
        // 保存到数据库
        int i = this.cameraConfigService.save(request);
        return "redirect:/file/list";
    }

    /**
     * 获取一个点位
     */
    @RequestMapping("/get")
    public Result<Camera> get(@RequestBody GetCameraRequest request) {
        Camera camera = cameraService.get(request.getId());
        if (camera == null)
            return ResultUtil.fail("点位不存在");

        return ResultUtil.success("成功", camera);
    }


/*    @PostMapping("/draw")
    public ResponseEntity<Boolean> save(String points) {
        System.out.println(points);
        List<Point> pointList = JSONObject.parseArray(points, Point.class);
        pointList.forEach(System.out::println);
        return ResponseEntity.ok(true);
    }*/

    @PostMapping("addroad")
    public Result addRoad(@RequestBody AddRoadRequest request) {
        //修改操作
        if (request.getRoad().getId() != null && !request.getRoad().getId().equals(""))
            return updateRoad(request);
        //获取点位
        Camera camera = cameraService.get(request.getCameraId());
        if (camera == null)
            return ResultUtil.fail("点位不存在，配置失败");

        //添加道路
        request.getRoad().setId(IdUtils.getId());

        camera.getRoads().add(request.getRoad());
        //保存点位
        boolean result = cameraService.update(camera);
        if (result)
            return ResultUtil.success();

        return ResultUtil.fail();
    }

    /**
     * =====手动绘制
     * 车道方向
     * 异常停车
     * 违禁车辆
     * 拥堵
     * =====复制功能
     * 违法行驶（复制异常停车绘制区域）
     * 行人非机动车、抛洒物（复制异常停车+违禁车辆绘制区域）
     * 施工区域（复制违禁车辆绘制区域）
     *
     * @param request
     * @return
     */
    private AddRoadRequest copy(AddRoadRequest request) {
        EventConfigs eventConfigs = request.getRoad().getEventConfigs();
        // 异常停车点位
        List<Area> illegalParkAreas = eventConfigs.getIllegalPark().getAreas();
        // 违禁车辆点位
        List<Area> forbidCarAreas = eventConfigs.getForbidCar().getAreas();
/*        //应急停车道
        OnEmergency onEmergency = eventConfigs.getIllegalDriving().getOnEmergency();
        if (onEmergency.getAreas().size()==0) {
            onEmergency.setAreas(illegalParkAreas);
        }*/
        // 逆行
        Retrograde retrograde = eventConfigs.getIllegalDriving().getRetrograde();
        if (retrograde.getAreas().size()==0) {
            retrograde.setAreas(illegalParkAreas);
        }
        // 行人非机动车
        PeopleOrNoVehicles peopleOrNoVehicles = eventConfigs.getPeopleOrNoVehicles();
/*        if (peopleOrNoVehicles.getPeople().getAreas().size()==0) {
            List<Area> peopleAreas = new ArrayList<>();
            peopleAreas.addAll(forbidCarAreas);
            peopleAreas.addAll(illegalParkAreas);
            peopleOrNoVehicles.getPeople().setAreas(peopleAreas);
        }*/
        if (peopleOrNoVehicles.getNoVehicles().getAreas().size()==0) {
            List<Area> peopleAreas = new ArrayList<>();
            peopleAreas.addAll(forbidCarAreas);
            peopleAreas.addAll(illegalParkAreas);
            peopleOrNoVehicles.getNoVehicles().setAreas(peopleAreas);
        }
        // 抛洒物
        ThrowThings throwThings = eventConfigs.getThrowThings();
        if (throwThings.getAreas().size()==0){
            List<Area> throwThingsAreas = new ArrayList<>();
            throwThingsAreas.addAll(forbidCarAreas);
            throwThingsAreas.addAll(illegalParkAreas);
            throwThings.setAreas(throwThingsAreas);
        }
        // 施工区域
        WorkSpace workSpace = eventConfigs.getWorkSpace();
        if (workSpace.getAreas().size()==0) {
            workSpace.setAreas(forbidCarAreas);
        }

        return request;
    }

    private Result updateRoad(@RequestBody AddRoadRequest request) {
        //获取点位
        Camera camera = cameraService.get(request.getCameraId());
        if (camera == null)
            return ResultUtil.fail("点位不存在，配置失败");
        AddRoadRequest copy = this.copy(request);
        List<Road> roadList = new ArrayList<>();
        for (Road road : camera.getRoads()) {
            if (road.getId().equals(copy.getRoad().getId())) {
                roadList.add(copy.getRoad());
            } else {
                roadList.add(road);
            }
        }

        camera.setRoads(roadList);
        //保存点位
        boolean result = cameraService.update(camera);
        if (result)
            return ResultUtil.success();

        return ResultUtil.fail();
    }

}
