package com.qsj.easydarwin.controller;

import com.alibaba.fastjson.JSON;
import com.qsj.easydarwin.config.Config;
import com.qsj.easydarwin.pojo.Camera;
import com.qsj.easydarwin.pojo.FileListResponse;
import com.qsj.easydarwin.pojo.PageResponse;
import com.qsj.easydarwin.pojo.VideoAnalysis;
import com.qsj.easydarwin.pojo.base.CameraDetails;
import com.qsj.easydarwin.pojo.base.Road;
import com.qsj.easydarwin.service.FileService;
import com.qsj.easydarwin.service.impl.CameraService;
import com.qsj.easydarwin.service.impl.VideoAnalysisService;
import com.qsj.easydarwin.util.ListPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/16 15:12
 */
@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    private Config config;
    @Autowired
    private FileService fileService;
    @Autowired
    private CameraService cameraService;
    @Autowired
    private VideoAnalysisService videoAnalysisService;

    @GetMapping("/group")
    public ModelAndView group() {
        ModelAndView mv = new ModelAndView("file-list");
        List<FileListResponse> responseList = this.fileService.getDirList();
        mv.addObject("fileList", responseList);
        return mv;
    }

    /**
     * 文件列表
     * @param group 分组查询
     * @param query 文件名查询
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return
     */
    @GetMapping("/list")
    public PageResponse<FileListResponse> list(String group, String query, Integer page, Integer pageSize) {

        String videoPath = this.config.getVideoPath();
        if (!StringUtils.isEmpty(group)) {
            videoPath += group;
        }
        List<FileListResponse> selectList = this.fileService.getFileList(videoPath);
        PageResponse<FileListResponse> pageResponse = new PageResponse<>();
        List<FileListResponse> responsesList = new ArrayList<>();
        // 搜索
        if (StringUtils.isEmpty(query)){
            responsesList = selectList;
        }else {
            for (FileListResponse file : selectList) {
                if (file.getPath().contains(query)) {
                    responsesList.add(file);
                }
            }
        }
        // 设置总条数
        pageResponse.setTotalCount(responsesList.size());
        // 分页
        if (page != null && pageSize != null) {
            List<FileListResponse> list = ListPageUtil.startPage(responsesList, page, pageSize);
            // 设置是否有数据
            for (FileListResponse file : list) {
                if (this.videoAnalysisService.getByVideoName(file.getPath()) != null) {
                    file.setVisited(true);
                }
            }
            pageResponse.setData(list);
        } else {
            pageResponse.setData(responsesList);
        }
        return pageResponse;
    }

    String defaultRoadConfig = "{\"id\":\"9b6ce8aa85d54a97b65b17a6fa1c36eb\",\"name\":\"默认配置\",\"direction\":{\"startPoint\":{\"xAxis\":0,\"yAxis\":0},\"endPoint\":{\"xAxis\":0,\"yAxis\":0}},\"eventConfigs\":{\"illegalPark\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[],\"num\":2},\"forbidCar\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[]},\"illegalDriving\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"retrograde\":{\"areas\":[],\"pixel\":200},\"onEmergency\":{\"areas\":[],\"num\":2}},\"peopleOrNoVehicles\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"people\":{\"areas\":[],\"num\":2},\"noVehicles\":{\"areas\":[],\"num\":2}},\"throwThings\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[],\"num\":2},\"crowed\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[]},\"workSpace\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[],\"count\":2},\"fire\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[]},\"fog\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[]},\"video\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"}}},\"trafficConfig\":{\"isOpen\":true,\"serviceTime\":{\"startTime\":\"00:00\",\"endTime\":\"23:59\"},\"areas\":[],\"interval\":15}}";

    /**
     * ajax请求 截图 根据rtsp自动生成点位
     *
     * @param request
     * @param videoUrl
     * @param time
     * @return
     */
    @GetMapping("/saveCurrentImage")
    public ResponseEntity<String> saveCurrentImage(HttpServletRequest request, String videoUrl, Long time) {

        // 返回图片相对路径
        String imageUrl = this.fileService.saveCurrentImage(videoUrl, time);
        // http视频链接转rtsp链接
        String rtsp = this.config.getRtspPath() + videoUrl.substring(this.config.getDomain().length() + "video".length());
        // 自动生成点位 视频url作为cameraId 和 roadId
        String httpImgUrl = this.config.getDomain()+ imageUrl;
        Camera camera = new Camera();
        camera.setId(videoUrl);
        CameraDetails cameraDetails = new CameraDetails();
        cameraDetails.setRtsp(rtsp);
        cameraDetails.setSceneImgUrl(httpImgUrl);
        cameraDetails.setSceneImg(this.config.getImagePath() + imageUrl.substring(7));
        camera.setCameraDetails(cameraDetails);
        // 自动生成road 默认配置
        Road road = JSON.parseObject(defaultRoadConfig, Road.class);
        road.setId(videoUrl);
        camera.setRoads(Arrays.asList(road));
        boolean b = this.cameraService.add(camera);
        if (b) {
            return ResponseEntity.ok(httpImgUrl);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
