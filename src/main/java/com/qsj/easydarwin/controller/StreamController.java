package com.qsj.easydarwin.controller;

import com.qsj.easydarwin.config.Config;
import com.qsj.easydarwin.util.FfmpegUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * 流管理
 * @author Q1sj
 * @version 1.0
 * @date 2020/7/10 9:28
 */
@Controller
@RequestMapping("/stream")
public class StreamController {
    public Map<String, FfmpegUtils> liveMap = new HashMap<>();
    @Autowired
    private Config config;



    @GetMapping("/start")
    public ResponseEntity<Void> startFile(String path){

        FfmpegUtils ffmpegUtils = new FfmpegUtils();
        String rtspSuffix = path.substring(config.getVideoPath().length() - 1);
        FfmpegUtils val = this.liveMap.get(rtspSuffix);
        if (val==null || val.isExit()) {
            liveMap.put(rtspSuffix, ffmpegUtils);
            String rtsp = this.config.getRtspPath() + rtspSuffix;
            new Thread(()-> ffmpegUtils.push(path, rtsp)).start();
        }
        return ResponseEntity.ok().build();
    }


    @GetMapping("/stop")
    public ResponseEntity<Void> stop(String path){
//        System.out.println(path);
        // 获取map中val 停止推流
        this.liveMap.get(path.substring(this.config.getVideoPath().length()-1)).setExit(true);
        return ResponseEntity.ok().build();
    }
}
